# AI13

## Installation

### Required
 * A Postgresql Database Server
 * JDK 8
 * NPM
 
### Database creation
Use script /Back/init_db.sql to create database and user for backend.

### Backend
 * Import the gradle project to download all dependencies.
 * Run the /Back/src/resources/sql/create_tables.sql to create current tables on project
 * Edit /Back/src/resources/application.properties to configure spring.datasource.url, spring.datasource.username, spring.datasource.password 
 if changes were made to /Back/init_db.sql
 * Run BackendApplication to run the Backend (with **JAVA 8** as some problems appears with more recent version)
 
### Frontend
 * In /Front, run *npm install* to install all node_modules
 * Run *npm start* to run Frontend

## Creating first Admin

To create the first admin, go to http://localhost:5000/swagger-ui.html, fill up the json in POST /signup (in Security), and execute.

This will create the admin.

Then, go to http://localhost:3000/ to sign in with the created account.