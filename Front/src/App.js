import React from 'react';
import './App.css';
import {Index} from "./pages/index"
import {Users} from "./pages/admin/Users"
import {Quizzes} from "./pages/admin/Quizzes"
import {Course} from "./pages/private/Course"
import {Courses} from "./pages/private/Courses"
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {Questions} from "./pages/admin/Questions";
import {CoursesResultsOverview} from "./pages/private/CoursesResultsOverview";
import {CourseResults} from "./pages/private/CourseResults";

function App() {
  return (
      <div className="App">
          {/*Ne rien faire d'autre qu'ajouter des routes ici*/}
          <BrowserRouter>
              <Switch>
                  <Route exact path="/" component={Index}/>
                  <Route exact path="/admin/users" component={Users}/>
                  <Route exact path="/admin/quizzes" component={Quizzes}/>
                  <Route exact path="/admin/quizzes/:id_quiz/questions" component={Questions}/>
                  <Route exact path="/private/quizzes/:id_quiz/questions" component={Course}/>
                  <Route exact path="/private/quizzes" component={Courses}/>
                  <Route exact path="/private/quizzes/quizzes_results_overview" component={CoursesResultsOverview}/>
                  <Route exact path="/private/quizzes/quizzes_results_overview/:id_course/details" component={CourseResults}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
}

export default App;
