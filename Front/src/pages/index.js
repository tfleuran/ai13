import React, {Component} from 'react';
import {Form, FormGroup, Label, Button, Input, Card, CardBody, CardFooter, CardHeader} from "reactstrap";
import Axios from 'axios';
import {Redirect} from "react-router-dom";

class Index extends Component {
    constructor(props){
        super(props);

        this.state = {
            username: null,
            password: null,
            loading: false,
            apiRequestStatus: null,
            admin: true
        }
    }

    handleFieldChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    doRedirect(role){
        console.log(role);
        if (role === "ROLE_ADMIN")
            window.location.replace("/admin/users");
        else
            window.location.replace("/private/quizzes");

    }

    handleSubmit(){
        let data = {
            'email': this.state.username,
            'password': this.state.password
        };
        this.setState({
            apiRequestStatus: "REQUEST",
        });
        Axios.post(window.API_URL + "login", data)
            .then((res) =>
            {
                let auth_token = res.headers.authorization.split(" ")[1];
                localStorage.setItem("auth_token", auth_token);
                this.setState({
                    apiRequestStatus: "SUCCESS"
                });

                this.doRedirect(res.data);
            })
            .catch((err) => {
                if (err.response !== undefined){
                    this.setState({
                        apiRequestStatus: "ERROR"
                    })
                }else{
                    this.setState({
                        apiRequestStatus: "ERROR"
                    })
                }
            })
    }

    render() {
        return(
            <div id="LoginForm">
                {this.state.apiRequestStatus === "SUCCESS" && this.state.admin &&
                <Redirect to="/admin/users"/>
                }
                {this.state.apiRequestStatus === "SUCCESS" && !this.state.admin &&
                <Redirect to="/private/quizzes"/>
                }
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                            <div className="text-center">
                                <Card>
                                    <Form onSubmit={(e)=>{
                                        e.preventDefault();
                                        this.handleSubmit();
                                    }}>
                                        <CardHeader><h1>Connexion</h1></CardHeader>
                                        <CardBody>
                                        <FormGroup>
                                            <Label for="username">Identifiant</Label>
                                            <Input id="usernameField" name="username" onChange={(e)=>{
                                                this.handleFieldChange(e)
                                            }}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="password">Mot de passe</Label>
                                            <Input id="passwordField" name="password" type="password" onChange={(e)=>{
                                                this.handleFieldChange(e)
                                            }}/>
                                        </FormGroup>
                                        </CardBody>
                                        <CardFooter><Button>Se connecter</Button></CardFooter>
                                    </Form>
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export {Index};