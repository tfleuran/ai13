import React, {Component} from 'react';
import {HeaderUser} from "../../components/headers/HeaderUser";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import Container from "reactstrap/es/Container";
import {Button, ButtonGroup, Table} from "reactstrap";
import Axios from "axios";

class CoursesResultsOverview extends Component{
    constructor(props){
        super(props);
        this.state ={
            authToken: null,
            headers: {
                title: "Questionnaire",
                subject: "Sujet",
                description: "Description",
                score: "Score",
                duration: "Durée du parcours",
                actionsButtons: "Actions"
            },
            rows: []
        }
        this.generateHeader = this.generateHeader.bind(this);
        this.generateList = this.generateList.bind(this);
    }

    generateHeader(){
        const {title, subject, description, score, duration, actionsButtons} = this.state.headers;
        return (
            <tr key="listQuizzesHeader" style={{textAlign : "center"}}>
                <th>{title}</th>
                <th>{subject}</th>
                <th>{description}</th>
                <th>{score}</th>
                <th>{duration}</th>
                <th>{actionsButtons}</th>
            </tr>
        );
    }

    generateList(){
        return this.state.rows.map((entry, index) => {
            const {id, title, subject, description, score, duration} = entry;

            return (
                <tr key={id} style={{textAlign : "center"}}>
                    <td>{title}</td>
                    <td>{subject}</td>
                    <td>{description}</td>
                    <td>{score}</td>
                    <td>{duration}</td>
                    <td>
                        <ButtonGroup>
                            <Button color="info" onClick={() => {this.showCourseResults(id)}}>Voir le détail du parcours</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }

    showCourseResults(id){
        //Switch to another page with React Router
        let path = "/private/quizzes/quizzes_results_overview/" + id + "/details";
        this.props.history.push(path);
    }

    render(){
        return(
            <>
                <HeaderUser/>
                <Row className="buttonRowSpacing">
                    <Col className="alignButtonFix">
                    </Col>
                </Row>
                <Container>
                    <Row>
                        <Col>
                            <Table className="headerFixAlign" bordered>
                                <thead>
                                    {this.generateHeader()}
                                </thead>
                                <tbody id="listBody">
                                    {this.generateList()}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }

    populateResultsList(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        this.setState({
            authToken: authTokenRetrieved
        });
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };
        Axios.get(window.API_URL + "private/courses/results", config)
            .then((res)=>{
                let rowsRetrieved = [];
                res.data.map((entry) => {
                    let formatedEntry = {};
                    formatedEntry.id = entry.id;
                    formatedEntry.title = entry.quizResource.title;
                    formatedEntry.subject = entry.quizResource.subject;
                    formatedEntry.description = entry.quizResource.description;
                    formatedEntry.score = entry.score;
                    let timeString = new Date(entry.duration).toISOString().slice(11,19); //Less than a day durations ONLY
                    formatedEntry.duration = timeString;
                    rowsRetrieved.push(formatedEntry);
                });
                this.setState({rows: rowsRetrieved});
            });
    }

    componentDidMount() {
        this.populateResultsList();
    }
}

export {CoursesResultsOverview};