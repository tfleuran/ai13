import React, {Component} from 'react';
import {HeaderUser} from "../../components/headers/HeaderUser";
import {Col, Container, Row, Table} from "reactstrap";
import Axios from "axios";

class CourseResults extends Component{
    constructor(props){
        super(props);
        this.state = {
            headers: {
                questionTitle: "Question",
                rightAnswer: "Bonne réponse",
                userAnswer: "Réponse donnée"
            },
            rows: [],
            courseId: null
        };

        this.generateHeader = this.generateHeader.bind(this);
        this.generateList = this.generateList.bind(this);
    }
    generateHeader(){
        const {questionTitle, rightAnswer, userAnswer} = this.state.headers;
        return (
            <tr key="listCourseResults" style={{textAlign : "center"}}>
                <th>{questionTitle}</th>
                <th>{rightAnswer}</th>
                <th>{userAnswer}</th>
            </tr>
        );
    }

    generateList(){
        let rightAnswerStyle = {color: "green"};
        let wrongAnswerStyle = {color: "red"};
        return this.state.rows.map((entry, index) => {
            const {questionTitle, rightAnswer, userAnswer} = entry;

            return (
                <tr key={"question"+index} style={{textAlign : "center"}}>
                    <td>{questionTitle}</td>
                    <td>{rightAnswer}</td>
                    <td style={rightAnswer.localeCompare(userAnswer) == 0 ? rightAnswerStyle : wrongAnswerStyle}>{userAnswer}</td>
                </tr>
            );
        })
    }

    populateResultsList(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        this.setState({
            authToken: authTokenRetrieved
        });
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };

        Axios.get(window.API_URL + "private/courses/" + this.props.match.params.id_course, config)
            .then((res)=>{
                let rowsRetrieved = [];
                res.data.results.map((entry) => {
                    let result = {
                        questionTitle: entry.question,
                        rightAnswer: entry.right_answer,
                        userAnswer: entry.user_answer
                    };
                    rowsRetrieved.push(result);
                    });
                this.setState({rows: rowsRetrieved, courseId: res.data.id});
            });
    }

    render(){
        return(
            <>
                <HeaderUser/>
                <Row className="buttonRowSpacing">
                    <Col className="alignButtonFix" />
                </Row>
                <Container>
                    <Row>
                        <Col>
                            <h1>{"Résultats du parcours n°" + this.state.courseId}</h1>
                            <Table className="headerFixAlign" bordered>
                                <thead>
                                {this.generateHeader()}
                                </thead>
                                <tbody id="listBody">
                                {this.generateList()}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }

    componentDidMount() {
        this.populateResultsList();
    }
}

export {CourseResults};