import React, {Component, useState} from 'react';
import {HeaderUser} from "../../components/headers/HeaderUser";
import Container from "reactstrap/es/Container";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import {Button, ButtonGroup, Table} from "reactstrap";
import Axios from "axios";
import {SearchQuizPopup} from "../../components/user/quiz/SearchQuizPopup";
import {Course} from "./Course";


class Courses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authToken: null,
            headers: {
                title: "Questionnaire",
                subject: "Sujet",
                description: "Description",
                actionsButtons: "Actions"
            },
            rows: [],
            searchQuizModal: false,
            quizID: null
        };

        this.toggleSearchQuizModal = this.toggleSearchQuizModal.bind(this);
        this.populateQuizList = this.populateQuizList.bind(this);
        this.openQuizPage = this.openQuizPage.bind(this);
    }

    generateHeader(){
        const {title, subject, description, actionsButtons} = this.state.headers;
        return (
            <tr key="listQuizzesHeader" style={{textAlign : "center"}}>
                <th>{title}</th>
                <th>{subject}</th>
                <th>{description}</th>
                <th>{actionsButtons}</th>
            </tr>
        )}

    generateList(){
        return this.state.rows.map((entry, index) => {
            const {id, title, subject, description} = entry;

            return (
                <tr key={id} style={{textAlign : "center"}}>
                    <td>{title}</td>
                    <td>{subject}</td>
                    <td>{description}</td>
                    <td>
                        <ButtonGroup>
                            <Button color="success" size="sm" onClick={()=>{this.openQuizPage(id)}}>Voir ce quiz</Button>
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return(
            <>
                <HeaderUser/>
                {this.showSearchQuizModal()}
                <div>
                    <Container>
                        <Row className="buttonRowSpacing">
                            <Col className="alignButtonFix">
                                <Button color="info" onClick={this.toggleSearchQuizModal}>Rechercher un questionnaire</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Table className="headerFixAlign" bordered>
                                    <thead>
                                    {this.generateHeader()}
                                    </thead>
                                    <tbody id="listBody">
                                    {this.generateList()}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        )
    }

    openQuizPage(id){
        this.setState({quizID: id});
        let path = "/private/quizzes/" + id + "/questions";

        //let history = useHistory();
        //const [quizID, setQuizID] = useState(id);
        this.props.history.push(path);
        /*return(
            <Course authtoken={this.state.authToken}
                    updateList={this.populateQuizList}
                    quizID={this.state.quizID}
            />
        )*/
    }

    populateQuizList(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        this.setState({
            authToken: authTokenRetrieved
        });
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };

        Axios.get(window.API_URL + "private/quizzes", config)
            .then((res)=>{
                this.setState({
                    rows: res.data
                });
            });
    }

    toggleSearchQuizModal() {
        this.setState({
            searchQuizModal: !this.state.searchQuizModal
        });
    }

    showSearchQuizModal(){
        return(
            <SearchQuizPopup authtoken={this.state.authToken}
                             updateList={this.populateQuizList}
                             toggleSearchSurveyModal={this.toggleSearchQuizModal}
                             searchQuizModal={this.state.searchQuizModal}
                             type={"user"}
            />
        );
    }

    componentDidMount() {
        this.populateQuizList();
    }
}


export {Courses};