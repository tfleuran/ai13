import React, {Component, useState,useEffect, useRef} from 'react';
import Axios from 'axios'
import {Button, ListGroup, ListGroupItem} from "reactstrap";

const Questions = (props) => {

    let question = props.question;
    return (
        <>
            <h1>{question.label}</h1>
            <ListGroup>
                {question.answers.map((a)=>{
                    return (
                        <ListGroupItem><Button onClick={()=>{
                            props.setResults(s => s.concat(a.id))
                        }}>
                            {a.label}
                        </Button></ListGroupItem>
                    )
                })}
            </ListGroup>
        </>
    )
};

const Results = (props) => {
    return (
        <>
            <h1>Résultats, {props.score}/{props.total} en {props.duration}s</h1>
            <h2>Vos réponses</h2>
            <ListGroup flush>
                {props.selectedAnswer.map((a)=>{
                    return (<ListGroupItem>{a}</ListGroupItem>)
                })}
            </ListGroup>
            <Button onClick={()=>{
                props.history.push("/private/quizzes/")
            }}>Terminer</Button>
        </>
    )
};

const Course = (props) => {
    //Hooks
    const [quizData, setQuizData] = useState(null);
    const [chronoTime, setChronoTime] = useState(0);
    const [pos, setPos] = useState(-1);
    const [selectedAnswers, setSelectedAnswers] = useState([]);
    const [chrono, setChrono] = useState(null);
    const [score, setScore] = useState(0);

    //Effect

    useEffect(() => {
        fecthData();
        const interval = setInterval(() => {
                setChronoTime(chronoTime => chronoTime + 1000);
        }, 1000);
        setChrono(interval);
        return () => clearInterval(interval);
    }, []);

    useEffect(()=>{
        if (pos >= 0 && quizData.questions[pos].answers.find(element => element.id === selectedAnswers[selectedAnswers.length-1]).correct) {
            setScore(score => score + 1);
        }
        setPos(p => p + 1);
    },[selectedAnswers]);

    useEffect(()=>{
        if(quizData !== null && pos === quizData.questions.length){
            let interval = chrono;
            clearInterval(interval);
            setChrono(null);
            postResults();
        }
    }, [pos]);

    useEffect(()=>{
        return(()=> {
            let interval = chrono;
            clearInterval(interval);
            setChrono(null);
            postResults()
        })
    }, []);


    //Data
    function fecthData(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };
        Axios.get(window.API_URL + "private/quizzes/" + props.match.params.id_quiz, config)
            .then((res)=>{
                console.log(res.data);
                setQuizData(res.data)
            })
            .catch(error => {
                console.error(error);
            });
    }

    //Logic
    function postResults(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };
        let results = {
            "answers": selectedAnswers,
            "duration": chronoTime,
            "quiz": parseInt(props.match.params.id_quiz, 10),
            "score": score //TODO to calculate
        };
        console.info(results);
        Axios.post(window.API_URL + "private/courses/", results, config)
            .then(response => {
                console.log("Succesfully sended results: " + response.data)
            })
            .catch(error => {
                console.error(error);
            });
    }

    //Render
    return(
        <>
            {quizData !== null && quizData.questions[pos] !== undefined &&
                <Questions
                    question={quizData.questions[pos]}
                    setResults={setSelectedAnswers}
                />
            }
            { quizData !== null && pos === quizData.questions.length &&
                <Results
                    selectedAnswer={selectedAnswers}
                    score={score}
                    total={quizData.questions.length}
                    history={props.history}
                    duration={chronoTime/1000}
                />
            }
        </>
    )
};

export {Course};