import React, {Component} from 'react';
import {HeaderAdmin} from "../../components/headers/HeaderAdmin";
import {Button, ButtonGroup, Input, Table} from "reactstrap";
import Container from "reactstrap/es/Container";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import Axios from "axios";
import { AddQuizPopup } from "../../components/admin/quiz/AddQuizPopup";
import {ChangeStatusPopup} from "../../components/admin/ChangeStatusPopup";
import {SearchQuizPopup} from "../../components/admin/quiz/SearchQuizPopup";


class Quizzes extends Component {
    constructor(props){
        super(props);
        this.state = {
            authToken: null,
            headers: {
                id: "#",
                title: "Questionnaire",
                subject: "Sujet",
                description: "Description",
                status: "Statut",
                actionsButtons: "Actions"
            },
            rows: [],
            addQuizModal: false,
            searchQuizModal: false,
            changeStatusQuizModal: false,
            quizToChangeStatus: { id: null }
        };

        this.toggleAddQuizModal = this.toggleAddQuizModal.bind(this);
        this.toggleSearchQuizModal = this.toggleSearchQuizModal.bind(this);
        this.toggleChangeStatusModal = this.toggleChangeStatusModal.bind(this);
        this.populateQuizList = this.populateQuizList.bind(this);
    }

    generateHeader(){
        const {id, title, subject, description, status, actionsButtons} = this.state.headers;
        return (
            <tr key="listQuizzesHeader" style={{textAlign : "center"}}>
                <th>{id}</th>
                <th>{title}</th>
                <th>{subject}</th>
                <th>{description}</th>
                <th>{status}</th>
                <th>{actionsButtons}</th>
            </tr>
        )}

    generateList(){
        return this.state.rows.map((entry, index) => {
            const {id, title, subject, description, status, activable} = entry;
            let statusCheckbox = <Input type="checkbox" className="btn-table" checked={status} style={{position: "initial", marginTop: "initial", marginLeft: "initial"}} inline={"true"} disabled/>;

            return (
                <tr key={id} style={{textAlign : "center"}}>
                    <th>{id}</th>
                    <td>{title}</td>
                    <td>{subject}</td>
                    <td>{description}</td>
                    <td>{statusCheckbox}</td>
                    <td>
                        <ButtonGroup>
                            <Button color="success" size="sm" onClick={()=>{this.openQuizPage(id)}}>Voir ce quiz</Button>
                            { entry.status === false &&
                            <Button color="primary"  size="sm" disabled={!activable} onClick={()=> this.toggleChangeStatusModal(entry)}>Activer le Quiz</Button>
                                
                            }
                            { entry.status === true &&
                                <Button color="danger"  size="sm" onClick={()=> this.toggleChangeStatusModal(entry)}>Désactiver le Quiz</Button>
                            }
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }

    openQuizPage(id){
        let path = "/admin/quizzes/" + id + "/questions";
        // let history = useHistory();
        this.props.history.push(path);
    }

    render() {
        return(
            <>
                <HeaderAdmin/>
                {this.showAddQuizModal()}
                {this.showSearchQuizModal()}
                {this.showChangeStatusQuizModal()}
                <div>
                    <Container>
                        <Row className="buttonRowSpacing">
                            <Col className="alignButtonFix">
                                <Button color="success" onClick={this.toggleAddQuizModal}>Ajouter un questionnaire</Button>
                                <Button color="info" onClick={this.toggleSearchQuizModal}>Rechercher un questionnaire</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Table className="headerFixAlign" bordered>
                                    <thead>
                                        {this.generateHeader()}
                                    </thead>
                                    <tbody id="listBody">
                                        {this.generateList()}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        )
    }

    populateQuizList(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        this.setState({
            authToken: authTokenRetrieved
        });
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };


        Axios.get(window.API_URL + "admin/quizzes", config)
            .then((res)=>{
                this.setState({
                    rows: res.data
                });
            });

    }

    toggleAddQuizModal(){
        this.setState({
            addQuizModal: !this.state.addQuizModal
        });
    }
    toggleChangeStatusModal(entry) {
        if(entry){
            const quizToChangeStatusInfo = {
                id: entry.id
            };
            this.setState({
                changeStatusQuizModal: !this.state.changeStatusQuizModal,
                quizToChangeStatus: quizToChangeStatusInfo
            });
        }
        else {
            this.setState({
                changeStatusQuizModal: !this.state.changeStatusQuizModal
            });
        }
    }

    toggleSearchQuizModal() {
        this.setState({
            searchQuizModal: !this.state.searchQuizModal
        });
    }

    showAddQuizModal(){
        return(
            <AddQuizPopup authtoken={this.state.authToken} updateList={this.populateQuizList}
                          toggleAddQuizModal={this.toggleAddQuizModal} addQuizModal={this.state.addQuizModal}/>
        );
    }

    showChangeStatusQuizModal() {
        return (
            <ChangeStatusPopup authtoken={this.state.authToken} updateList={this.populateQuizList}
                               context = {{component : "Quiz"}}
                               elementToChangeStatus={this.state.quizToChangeStatus}
                               toggleChangeStatusModal={this.toggleChangeStatusModal}
                               changeStatusModal={this.state.changeStatusQuizModal}/>
        );
    }

    showSearchQuizModal(){
        return(
            <SearchQuizPopup authtoken={this.state.authToken}
                             updateList={this.populateQuizList}
                             toggleSearchSurveyModal={this.toggleSearchQuizModal}
                             searchQuizModal={this.state.searchQuizModal}
                             type={"admin"}
            />
        );
    }

    componentDidMount() {
        this.populateQuizList();
    }

}

export {Quizzes};