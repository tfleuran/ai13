import React, {Component} from 'react';
import {HeaderAdmin} from "../../components/headers/HeaderAdmin";
import {Table, Pagination} from 'reactstrap';
import { Button } from 'reactstrap';
import { ButtonGroup, Input } from 'reactstrap';
import Container from "reactstrap/es/Container";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import { AddUserPopup } from "../../components/admin/user/AddUserPopup";
import Axios from 'axios';
import {ModifyUserPopup} from "../../components/admin/user/ModifyUserPopup";
//import {DeleteUserPopup} from "../../components/admin/user/DeleteUserPopup";
import {ChangeStatusPopup} from "../../components/admin/ChangeStatusPopup";
import {SeeUserStatsPopup} from "../../components/admin/user/SeeUserStatsPopup";
import {ShowUserPopup} from "../../components/admin/user/ShowUserPopup";
import PaginationItems from "../../components/pagination";
// import Pagination from "../../components/pagination";

class Users extends Component {
    constructor(props){
        super(props);
        this.state= {
            authToken: null,
            headers: {
                admin: "Administrateur",
                company: "Société",
                creationDate: "Date de création",
                email: "Identifiant (mail)",
                name: "Nom",
                phoneNumber: "Téléphone",
                status: "Statut",
                actionsButtons: "Actions"
            },
            rows : [],
            modificationModal: false,
            showUserModal: false,
            addUserModal: false,
            changeStatusUserModal: false,
            userToModify: {
                userId: null,
                name: null,
                company: null,
                phoneNumber: null
            },
            userToShow: {
                userId: null,
                name: null,
                company: null,
                phoneNumber: null
            },
            userToChangeStatus: {id: null},
            userToSee: null,
            seeUserStats: false,
            currentPage: 0
        };
        this.toggleModificationModal = this.toggleModificationModal.bind(this);
        this.toggleChangeStatusModal = this.toggleChangeStatusModal.bind(this);
        this.toggleShowUserModal = this.toggleShowUserModal.bind(this);
        this.toggleAddUserModal = this.toggleAddUserModal.bind(this);
        this.toggleSeeUserStatsModal = this.toggleSeeUserStatsModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.populateUsersList = this.populateUsersList.bind(this);
        this.setCurrentPage = this.setCurrentPage.bind(this);
    }

    handleSubmit(event){
        let data = {
            'name': this.state.name,
            'company': this.state.company,
            'phoneNumber': this.state.phoneNumber
        };
        console.log(window.API_URL)
        Axios.post(window.API_URL, data)
            .then((res) =>
            {
                this.setState({
                    apiRequestStatus: "SUCCESS"
                });
            })
            .catch(() =>
                this.setState({
                    apiRequestStatus: "ERROR"
                })
            )
    }

    generateHeader(){
        const {admin, company, creationDate, email, name, phoneNumber, status, actionsButtons} = this.state.headers;
        return (
            <tr key="listUsersHeader">
                <th>{email}</th>
                <th>{name}</th>
                <th>{company}</th>
                <th>{creationDate}</th>
                <th>{phoneNumber}</th>
                <th>{admin}</th>
                <th>{status}</th>
                <th>{actionsButtons}</th>
            </tr>
        )}

    generateList(){
        return this.state.rows
            .slice(this.state.currentPage * 10, (this.state.currentPage + 1) * 10)
            .map((entry, index) => {
            const {admin, company, creationDate, email, name, phoneNumber, status} = entry;
            let statusCheckbox = <Input type="checkbox" id="statusCheckbox" checked={status} disabled/>,
                adminCheckbox = <Input type="checkbox" id="adminCheckbox" checked={admin} disabled/>;

            return (
                <tr key={index}>
                    <td>{email}</td>
                    <td>{name}</td>
                    <td>{company}</td>
                    <td>{creationDate}</td>
                    <td>{phoneNumber}</td>
                    <td>{adminCheckbox}</td>
                    <td>{statusCheckbox}</td>
                    <td>
                        <ButtonGroup>
                            {/*<Button color="primary" onClick={() => this.toggleShowUserModal(entry)}>Voir</Button>*/}
                            <Button color="primary" onClick={() => this.toggleSeeUserStatsModal(email)}>Stats</Button>
                            <Button color="info" onClick={()=> this.toggleModificationModal(entry)}>Modifier</Button>
                            {/*<Button color="danger" onClick={()=> this.toggleChangeStatusModal(entry)}>Supprimer</Button>*/}
                            { entry.status === false &&
                            <Button color="primary"  size="sm" onClick={()=> this.toggleChangeStatusModal(entry)}>Activer l' utilisateur</Button>

                            }
                            { entry.status === true &&
                            <Button color="danger"  size="sm" onClick={()=> this.toggleChangeStatusModal(entry)}>Désactiver l' utilisateur</Button>
                            }
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }

    setCurrentPage = (i) =>{
        this.setState({
            currentPage: i
        })
    };

    render() {
        return(
            <>
                <HeaderAdmin/>
                {this.showAddUserModal()}
                {this.showModifyUserModal()}
                {this.showChangeStatusUserModal()}
                {this.showShowUserModal()}
                {this.showUserStats()}
                <div>
                    <Container>
                        <Row className="buttonRowSpacing">
                            <Col className="alignButtonFix">
                                <Button color="success" onClick={this.toggleAddUserModal}>Ajouter utilisateur</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Table className="headerFixAlign" bordered>
                                    <thead>
                                    {this.generateHeader()}
                                    </thead>
                                    <tbody id="listBody">
                                    {this.generateList()}
                                    </tbody>
                                </Table>
                                <Pagination aria-label="Page navigation example">
                                <PaginationItems
                                    pagesCount={Math.floor(this.state.rows.length / 10) + (this.state.rows.length % 10 !== 0 ? 1 : 0)}
                                    currentPage={this.state.currentPage}
                                    setCurrentPage={this.setCurrentPage}
                                />
                                </Pagination>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        )
    }

    populateUsersList(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        this.setState({
            authToken: authTokenRetrieved
        });
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };

        Axios.get(window.API_URL + "admin/users/all", config)
            .then((res)=>{
                this.setState({
                    rows: res.data
                });
            });
    }

    componentDidMount() {
        this.populateUsersList();
    }

    showAddUserModal(){
        return(
            <AddUserPopup authtoken={this.state.authToken} updateList={this.populateUsersList}
                          toggleAddUserModal={this.toggleAddUserModal} addUserModal={this.state.addUserModal}/>
        );
    }

    showModifyUserModal(){
        return(
            <ModifyUserPopup authtoken={this.state.authToken} updateList={this.populateUsersList}
                             userToModify={this.state.userToModify}
                             toggleModificationModal={this.toggleModificationModal} modifyUserModal={this.state.modificationModal} />
        );
    }

    showChangeStatusUserModal() {
        return (
            <ChangeStatusPopup authtoken={this.state.authToken} updateList={this.populateUsersList}
                               context = {{component : "User"}}
                               elementToChangeStatus={this.state.userToChangeStatus}
                               toggleChangeStatusModal={this.toggleChangeStatusModal}
                               changeStatusModal={this.state.changeStatusUserModal}/>
        );
    }

    showShowUserModal(){
        return(
            <ShowUserPopup   authtoken={this.state.authToken} userToShow={this.state.userToShow}
                             toggleShowUserModal={this.toggleShowUserModal} showUserModal={this.state.showUserModal} />
        );
    }

    showUserStats(){
        return(
            <SeeUserStatsPopup toggleUserStatsPopup={this.toggleSeeUserStatsModal}
                                userToSee={this.state.userToSee}
                               seeUserStats={this.state.seeUserStats}/>
        );
    }

    toggleModificationModal(entry){
        if(entry){
            const userToModifyInfo = {
                userId: entry.email,
                name: entry.name,
                company: entry.company,
                phoneNumber: entry.phoneNumber
            };
            this.setState({
                modificationModal: !this.state.modificationModal,
                userToModify: userToModifyInfo
            });
        } else {
            this.setState({
                modificationModal: !this.state.modificationModal
            });
        }
    }

    toggleChangeStatusModal(entry) {
        if(entry){
            if (entry.admin === false) {
                const userToChangeStatusInfo = {
                    id: entry.email
                };
                this.setState({
                    changeStatusUserModal: !this.state.changeStatusUserModal,
                    userToChangeStatus: userToChangeStatusInfo
                });
            } else {
                // TODO     on pourrait peut etre faire mieux en n'affichant pas le bouton "supprimer" si c'est un admin
                alert("Vous ne pouvez faire cette action sur un admin.")
            }
        }
        else {
            this.setState({
                changeStatusUserModal: !this.state.changeStatusUserModal
            });
        }
    }

    toggleAddUserModal(){
        this.setState({
            addUserModal: !this.state.addUserModal
        });
    }

    toggleSeeUserStatsModal(userMail){
        this.setState({
            userToSee: userMail,
            seeUserStats: !this.state.seeUserStats
        });
    }

    toggleShowUserModal(entry) {
        if(entry){
            const userToShowInfo = {
                userId: entry.email,
                name: entry.name,
                company: entry.company,
                phoneNumber: entry.phoneNumber
            };
            this.setState({
                showUserModal: !this.state.showUserModal,
                userToShow: userToShowInfo
            });
        } else {
            this.setState({
                showUserModal: !this.state.showUserModal
            });
        }
    }
}

export {Users};