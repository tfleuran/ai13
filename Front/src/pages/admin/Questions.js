import React, {Component} from 'react';
import { Table } from 'reactstrap';
import { Button } from 'reactstrap';
import { ButtonGroup, Input } from 'reactstrap';
import Container from "reactstrap/es/Container";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import Axios from 'axios'
import {AddQuestionModal} from "../../components/admin/quiz/AddQuestionModal";
import { HeaderAdmin } from '../../components/headers/HeaderAdmin';
import {ModifyQuestionModal} from "../../components/admin/quiz/ModifyQuestionModal";
import {ChangeStatusPopup} from "../../components/admin/ChangeStatusPopup";

class Questions extends Component {
    constructor(props){
        super(props);
        /*
        FIXME: Remove fake elements in the list when backend queries are available
        */
        this.state= {
            authToken: null,
            headers: {
                id: "Numero",
                label: "Énoncé",
                status: "Status",
                actionsButtons: "Actions possibles"
            },
            quizInfo: {
                quizName: "myQuiz",
                quizSubject: "Java test"
            },
            questionsList: [],
            questionToModify: {
                label: null,
                position: 0,
                answers: [],
                status: false,
                questionId: null
            },
            addQuestionModal: false,
            modifyQuestionModal: false,
            changeStatusQuestionModal: false,
            questionToChangeStatus: { id: null }
        };

        this.populateQuestionsList = this.populateQuestionsList.bind(this);
        this.toggleAddQuestionModal = this.toggleAddQuestionModal.bind(this);
        this.toggleModifyQuestionModal = this.toggleModifyQuestionModal.bind(this);
        this.toggleChangeStatusModal = this.toggleChangeStatusModal.bind(this);
    }

    generateHeader(){
        const {id, label, status, actionsButtons} = this.state.headers;
        return (
            <tr key="listQuestionsHeader">
                <th>{id}</th>
                <th>{label}</th>
                <th>{status}</th>
                <th>{actionsButtons}</th>
            </tr>
        );
    }

    generateList(){
        return this.state.questionsList.map((entry, index) => {
            const {position, label, status, answers, id, activable} = entry;

            return (
                <tr key={index}>
                    <td>{position}</td>
                    <td>{label}</td>
                    <td><Input type="checkbox" className="btn-table"
                               checked={status} style={{position: "initial", marginTop: "initial", marginLeft: "initial"}}
                               inline={"true"} disabled/></td>
                    <td>
                        <ButtonGroup>
                            {/*<Button color="primary">Voir</Button>*/}
                            <Button color="info" onClick={()=>{this.toggleModifyQuestionModal(entry)}}>Modifier</Button>
                            { entry.status === true &&
                            <Button color="danger" size="sm" onClick={()=> this.toggleChangeStatusModal(entry)}>Désactiver</Button>
                            }
                            {entry.status === false &&
                            <Button color="primary" disabled={!activable} onClick={()=> this.toggleChangeStatusModal(entry)}>Activer</Button>
                            }
                        </ButtonGroup>
                    </td>
                </tr>
            );
        });
    }

    render(){
        return(
            <>
                <HeaderAdmin />
                {this.showAddQuestionModal()}
                {this.showModifyQuestionModal()}
                {this.showChangeStatusQuizModal()}
                <h1>Quiz {this.props.match.params.id_quiz} !</h1>
                <div>
                    <Container>
                        <Row className="buttonRowSpacing">
                            <Col className="alignButtonFix">
                                <Button color="success" onClick={this.toggleAddQuestionModal}>Ajouter une question</Button>
                                {/*<Button color="info">Voir les informations du questionnaire</Button>*/}
                                {/*<Button color="danger">Supprimer le questionnaire</Button>*/}
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Table className="headerFixAlign" bordered>
                                    <thead>
                                        {this.generateHeader()}
                                    </thead>
                                    <tbody id="listBody">
                                        {this.generateList()}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </>
        );
    }

    populateQuestionsList(){
        let authTokenRetrieved = localStorage.getItem("auth_token");
        this.setState({
            authToken: authTokenRetrieved
        });
        let config = {
            headers : {"Authorization": "Bearer " + authTokenRetrieved}
        };

        let id = this.props.match.params.id_quiz;
        Axios.get(window.API_URL + "admin/quizzes/" + id + "/questions", config)
        .then((res)=>{
            this.setState({
                questionsList: res.data
            });
        });
    }

    toggleAddQuestionModal(){
        this.setState(
            {addQuestionModal : !this.state.addQuestionModal}
        );
    }

    showAddQuestionModal(){
        return(
            <AddQuestionModal authtoken={this.state.authToken}
                              updateList={this.populateQuestionsList}
                              associatedQuiz={this.props.match.params.id_quiz}
                              toggleAddQuestionModal={this.toggleAddQuestionModal}
                              addQuestionModal={this.state.addQuestionModal} />
        );
    }

    showChangeStatusQuizModal() {
        return (
            <ChangeStatusPopup authtoken={this.state.authToken} updateList={this.populateQuestionsList}
                               context = {{component : "Question"}}
                               elementToChangeStatus={this.state.questionToChangeStatus}
                               toggleChangeStatusModal={this.toggleChangeStatusModal}
                               changeStatusModal={this.state.changeStatusQuestionModal}/>
        );
    }

    toggleChangeStatusModal(entry) {
        if(entry){
            const questionToChangeStatusInfo = {
                id: entry.id
            };
            this.setState({
                changeStatusQuestionModal: !this.state.changeStatusQuestionModal,
                questionToChangeStatus: questionToChangeStatusInfo
            });
        }
        else {
            this.setState({
                changeStatusQuestionModal: !this.state.changeStatusQuestionModal
            });
        }
    }

    toggleModifyQuestionModal(entry){
        if(entry){
            const questionToModifyInfo = {
                label: entry.label,
                position: entry.position,
                answers: entry.answers,
                status: entry.status,
                questionId: entry.id
            };
            this.setState({
                modifyQuestionModal: !this.state.modifyQuestionModal,
                questionToModify: questionToModifyInfo
            });
        } else {
            this.setState({modifyQuestionModal: !this.state.modifyQuestionModal});
        }
    }

    showModifyQuestionModal(){
        return(
            <ModifyQuestionModal authtoken={this.state.authToken}
                                 updateList={this.populateQuestionsList}
                                 associatedQuiz={this.props.match.params.id_quiz}
                                 toggleModifyQuestionModal={this.toggleModifyQuestionModal}
                                 modifyQuestionModal={this.state.modifyQuestionModal}
                                 questionToModify={this.state.questionToModify}/>
        );
    }
    componentDidMount(){
        this.populateQuestionsList();
    }
}

export {Questions};