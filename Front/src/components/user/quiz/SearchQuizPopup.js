import React, {Component} from 'react';
import Axios from "axios";
import {Modal} from "reactstrap";

class SearchQuizPopup extends Component {
    constructor(props) {
        super(props);
        this.state =  {
            formField : {
                survey: "",
                subject: "",
                status: true,
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        let formFieldUpdated = this.state.formField;
        if(event.target.type === 'checkbox'){
            formFieldUpdated[event.target.name] = event.target.checked;
            this.setState({formField: formFieldUpdated});
        } else {
            formFieldUpdated[event.target.name] = event.target.value;
            this.setState({formField: formFieldUpdated});
        };
    }

    handleSubmit(event){
        let config = {
            headers : {"Authorization": "Bearer " + this.props.authtoken}
        };

        let surveyData = {
            'survey': this.state.formField.survey,
            'subject': this.state.formField.subject
        };

        if (this.state.props.type === "admin")
            surveyData.status = this.state.formField.status
        
        let self = this;

        // TODO     Dans le back, "surveys" ou autre nom pour les questionnaires n'a pas encore d'API.
        // TODO     Le lien dans Axios.get() sera donc surement à changer.
        Axios.get(window.API_URL + "admin/surveys" , surveyData, config )
            .then(response => {
                self.props.toggleSearchSurveyModal();
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
    }

    render(){
        // TODO aller chercher dans la db la liste des subjects
        // TODO list pas en react strap... pas trouvé comment la faire :'(
        return(
            <Modal isOpen={this.props.searchSurveyModal} toggle={this.toggleSearchSurveyModal}>
                <img src="https://media.giphy.com/media/NTddjTZTeOmXK/giphy.gif" alt="Nope..."/>
            </Modal>

        )
    }
}
export {SearchQuizPopup};