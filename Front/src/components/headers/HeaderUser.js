import React, {useState} from 'react';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarText, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {Link} from "react-router-dom";

const HeaderUser  = () => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return(
        <Navbar color="light" light expand="md">
            <NavbarBrand>AI13</NavbarBrand>
            <NavbarToggler onClick={toggle} navbar/>
            <Collapse isOpen={isOpen} navbar>
                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <NavLink href="/private/quizzes">Liste des questionnaires</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/private/quizzes/quizzes_results_overview">Liste des résultats obtenus</NavLink>
                    </NavItem>
                </Nav>
                <NavbarText><Link to="/"><a>Login Page</a></Link></NavbarText>
            </Collapse>
        </Navbar>
    )
};

export {HeaderUser};