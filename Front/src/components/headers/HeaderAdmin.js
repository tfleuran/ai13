import React, {useState} from 'react';
import {Nav, NavItem, Navbar, NavbarToggler, Collapse, NavbarText, NavLink, NavbarBrand} from "reactstrap";
import {Link} from "react-router-dom";
//import {AdminUserList} from "../AdminUserList";

const HeaderAdmin = () => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return(
            <Navbar color="light" light expand="md">
                <NavbarBrand>AI13</NavbarBrand>
                <NavbarToggler onClick={toggle} navbar/>
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink href="/admin/quizzes">Gestion des questionnaires</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/admin/users">Gestion des utilisateurs</NavLink>
                            </NavItem>
                        </Nav>
                        <NavbarText><Link to="/"><a>Login Page</a></Link></NavbarText>
                    </Collapse>
            </Navbar>
    )
};

export {HeaderAdmin};