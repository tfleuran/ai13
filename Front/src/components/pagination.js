import React, {useState} from 'react';
import {PaginationItem, PaginationLink} from 'reactstrap';

const PaginationItems = (props) => {

    const pagesCount = props.pagesCount;
    const currentPage = props.currentPage;

    function handlePageClick(e, i){
        e.preventDefault();
        props.setCurrentPage(i);
    }

    return(
        <>
                {[...Array(pagesCount)].map((page, i) => (
                    <PaginationItem active={i === currentPage} key={i}>
                        <PaginationLink onClick={e => handlePageClick(e, i)} href="#">
                            {i + 1}
                        </PaginationLink>
                    </PaginationItem>
                ))}
        </>
    )
};

export default PaginationItems;