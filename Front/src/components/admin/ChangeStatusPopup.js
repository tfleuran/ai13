import React, {Component} from 'react';
import Axios from "axios";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class ChangeStatusPopup extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        let config = {
            headers: {"Authorization": "Bearer " + this.props.authtoken}
        };
        let self = this;
        self.props.toggleChangeStatusModal();
        let URL = "";
        switch (this.props.context.component) {
            case "Quiz":
                URL = window.API_URL + "admin/quizzes/" + this.props.elementToChangeStatus.id + "/status";
                break;
            case "Question":
                URL = window.API_URL + "admin/questions/" + this.props.elementToChangeStatus.id + "/status";
                break;
            case "User":
                URL = window.API_URL + "admin/users/" + this.props.elementToChangeStatus.id + "/status";
                break;
            default:
                console.error("Unexpected component.");
                break;

        }
        // URL = this.props.context.component === "Quiz" ? window.API_URL + "admin/quizzes/" + this.props.elementToChangeStatus.id + "/status" :
        self.props.toggleChangeStatusModal(null);
        Axios.patch(URL, {}, config)
            .then(response => {
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
    }

    render() {
        return (
            <Modal isOpen={this.props.changeStatusModal} toggle={() => this.props.toggleChangeStatusModal(null)}>
                <ModalHeader>Changer le status d'un {this.props.context.component}</ModalHeader>
                <ModalBody>
                    <p>Voulez vous changer le
                        status {(this.props.context.component === "Quiz" && "du Quiz") || (this.props.context.component === "Question" && "de la question") ||
                        (this.props.context.component === "User" && "de l'utilisateur")
                    }?</p>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => {
                        e.preventDefault();
                        this.handleSubmit(e)
                    }}>Valider</Button>
                    <Button color="warning" onClick={() => this.props.toggleChangeStatusModal(null)}>Annuler</Button>
                </ModalFooter>
            </Modal>

        )
    }
}

export {ChangeStatusPopup};