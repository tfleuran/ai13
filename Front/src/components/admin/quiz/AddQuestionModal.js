import React, {Component} from 'react';
import Axios from "axios";
import {Button, Form, FormGroup, Input, InputGroup, InputGroupAddon, InputGroupText,
    Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class AddQuestionModal extends Component {
    constructor(props) {
        super(props);
        this.state =  {
            formField : {
                label: null,
                position: 0,
                answers: [],
                status: false
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.addNewAnswerField = this.addNewAnswerField.bind(this);
    }

    addNewAnswerField(){
        let updateState = this.state;
        updateState.formField.answers.push({answerValue: "", isRightAnswer: false});
        this.setState(updateState);
    }

    generateAnswerFields(){
        return this.state.formField.answers.map((entry, index) => {
           return (
               <FormGroup key={index}>
                   <InputGroup>
                       <InputGroupAddon addonType="prepend">
                           <InputGroupText>
                               <Input addon type="checkbox" name={"answer-" + index + "-isCorrect"}
                                      aria-label="Checkbox for following text input" onChange={this.handleChange} />
                           </InputGroupText>
                       </InputGroupAddon>
                       <Input type="text" name={"answer-" + index} id={"answer-" + index}
                              onChange={this.handleChange} placeholder="Réponse" />
                   </InputGroup>
               </FormGroup>
           );
        });
    }

    handleChange(event){
        let regex = "\\d+";
        let formFieldUpdated = this.state.formField;
        if(event.target.name.includes("answer")){
            let indexToModify = event.target.name.match(regex)[0];
            if(event.target.type === 'checkbox'){
                formFieldUpdated.answers[indexToModify].isRightAnswer = event.target.checked;
                this.setState({formField: formFieldUpdated});
            } else {
                formFieldUpdated.answers[indexToModify].answerValue = event.target.value;
                this.setState({formField: formFieldUpdated});
            }
        } else if(event.target.type === 'checkbox'){
            formFieldUpdated[event.target.name] = event.target.checked;
            this.setState({formField: formFieldUpdated});
        } else {
            formFieldUpdated[event.target.name] = event.target.value;
            this.setState({formField: formFieldUpdated});
        };
    }

    handleSubmit(event){
        let config = {
            headers : {"Authorization": "Bearer " + this.props.authtoken}
        };

        let countRightAnswers = 0;
        this.state.formField.answers.forEach((answer) => {
            if(answer.isRightAnswer){
                countRightAnswers ++;
            }
        });
        switch(countRightAnswers) {
            case 0:
                console.error("No right answer");
                break;
            case 1:
                console.log("One right answer. Can proceed");
                let self = this;
                let questionData = {
                    'label': this.state.formField.label,
                    'position': parseInt(this.state.formField.position),
                    'status': this.state.formField.status
                };

                Axios.post(window.API_URL + "admin/quizzes/" + self.props.associatedQuiz, questionData, config)
                    .then((requestAnswer) => {
                        console.log("Sucesfully created question, now going for answers");
                        let questionId = requestAnswer.data.id;
                        self.state.formField.answers.forEach((entry, index) => {
                            let answerToAdd = {
                                'correct': entry.isRightAnswer,
                                'label': entry.answerValue,
                                'position': 0,
                                'status': true,
                                'id': index
                            };
                            Axios.post(window.API_URL + "admin/questions/" + questionId + "/answers", answerToAdd, config)
                                .then(()=> {
                                    console.log("Sucesfully created answer");
                                    self.props.toggleAddQuestionModal();
                                    self.props.updateList();
                                })
                                .catch((error) => {
                                    console.error(error);
                                });
                        });
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                break;
            default:
                console.error("Amount of right answers is not right, can't continue");
        }
    }

    render(){
        return(
            <Modal isOpen={this.props.addQuestionModal} toggle={this.toggleAddQuestionModal}>
                <ModalHeader>Ajouter une question au quiz {this.props.associatedQuiz}</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="questionId">Intitulé de la question</Label>
                            <Input type="text" name="label" id="questionId"
                                   onChange={this.handleChange} placeholder="Quelle est la question ?" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="position">Position dans le quiz</Label>
                            <Input type="number" name="position" id="position" onChange={this.handleChange}
                                   placeholder="3"/>
                        </FormGroup>
                        <FormGroup check inline>
                            <Label for="questionStatus">Active ?</Label>
                            <Input type="checkbox" name="status"
                                   onChange={this.handleChange} id="questionStatus"/>
                        </FormGroup>
                        <FormGroup>
                            <Label>Réponses (cocher la bonne réponse)</Label>
                            <Button onClick={this.addNewAnswerField}>+</Button>
                            {this.generateAnswerFields()}
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}>Valider</Button>
                    <Button color="warning" onClick={this.props.toggleAddQuestionModal}>Annuler</Button>
                </ModalFooter>
            </Modal>

        )
    }
}
export {AddQuestionModal};