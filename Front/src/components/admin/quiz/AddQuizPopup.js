import React, { Component } from 'react';
import Axios from "axios";
import { Button, Col, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row } from "reactstrap";

class AddQuizPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authToken: null,
            formField: {
                quiz: null,
                selectedSubject: null,
                newSubject: null,
                description: null
            },
            subjects: [],
            subjectsFetched: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        let formFieldUpdated = this.state.formField;
        if (event.target.type === 'checkbox') {
            formFieldUpdated[event.target.name] = event.target.checked;
            this.setState({ formField: formFieldUpdated });
        } else {
            formFieldUpdated[event.target.name] = event.target.value;
            this.setState({ formField: formFieldUpdated });
        }
    }

    handleSubmit(event) {
        let config = {
            headers: { "Authorization": "Bearer " + this.props.authtoken }
        };
        let quizSubject;
        if(this.state.formField.newSubject && this.state.formField.newSubject.length !== 0){
            quizSubject = this.state.formField.newSubject;
        } else {
            quizSubject = this.state.formField.selectedSubject;
        }

        let surveyData = {
            "description": this.state.formField.description,
            "subject": quizSubject,
            "title": this.state.formField.quiz
        };
        let self = this;

        console.log(surveyData);

        Axios.post(window.API_URL + "admin/quizzes", surveyData, config)
            .then(response => {
                console.log("Succesfully added Survey: " + response.data);

                // Clear form field
                self.setState({
                    formField: {
                        quiz: null,
                        selectedSubject: null,
                        newSubject: null,
                        description: null,
                        status: false
                    }
                });
                self.props.toggleAddQuizModal();
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
    }

    render() {
        // TODO aller chercher dans la db la liste des subjects
        return (
            <Modal isOpen={this.props.addQuizModal} toggle={this.toggleAddQuizModal}>
                <ModalHeader>Ajouter un questionnaire</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="quizId">Quiz</Label>
                            <Input type="text" name="quiz" id="quizId" onChange={this.handleChange}
                                placeholder="WebServices" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input type="text" name="description" id="description" onChange={this.handleChange}
                                   placeholder="WebServices dans une Application Web" />
                        </FormGroup>
                        <FormGroup>
                            <Row form>
                                <Col md={5}>
                                    <FormGroup>
                                        <Label for="subjectSelect">Choisir un sujet</Label>
                                        <Input type="select" name="selectedSubject" id="subjectSelect" onChange={this.handleChange}>
                                            {this.state.subjects.map((entry, index) => <option key={index}>{entry.label}</option>)}
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col md={1}>
                                    <p>OU</p>
                                </Col>
                                <Col md={6}>
                                    <FormGroup>
                                        <Label for="newSurveySubject">Nouveau sujet</Label>
                                        <Input type="text" name="newSubject" id="newSurveySubject"
                                            onChange={this.handleChange} placeholder="JavaScript ?" />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => { e.preventDefault(); this.handleSubmit(e) }}>Valider</Button>
                    <Button color="warning" onClick={this.props.toggleAddQuizModal}>Annuler</Button>
                </ModalFooter>
            </Modal>

        )
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.addQuizModal && !this.state.subjectsFetched){
            let authTokenRetrieved = localStorage.getItem("auth_token");
            this.setState({
                authToken: authTokenRetrieved
            });
            let config = {
                headers : {"Authorization": "Bearer " + authTokenRetrieved}
            };
            Axios.get(window.API_URL + "subjects", config)
                .then((result) => {
                    let formFields  = this.state.formField;
                    if (result.data.length > 0){
                        formFields["selectedSubject"] = result.data[0].label;
                    }
                    this.setState({
                        subjects: result.data,
                        formField : formFields
                    })
                })
                .catch((e) => console.log(e.toString()));
            this.setState({
                subjectsFetched: true
            })
        }
        else if(!this.props.addQuizModal && this.state.subjectsFetched){
            this.setState({
                subjectsFetched: false
            })
        }

    }
}
export { AddQuizPopup };