import React, {Component} from 'react';
import Axios from "axios";
import {
    Button,
    Form,
    FormGroup,
    Input,
    InputGroup, InputGroupAddon, InputGroupText,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";

class ModifyQuestionModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            formField: {
                label: null,
                position: 0,
                answers: [],
                status: false
            }
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.addNewAnswerField = this.addNewAnswerField.bind(this);
    }

    addNewAnswerField(){
        let updateState = this.state;
        updateState.formField.answers.push({answerValue: "", correct: false, status: true});
        this.setState(updateState);
    }

    generateAnswerFields(){
        return this.props.questionToModify.answers.map((entry, index) => {
            return (
                <FormGroup key={index}>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                                <Input addon type="checkbox" name={"answer-" + index + "-isCorrect"}
                                       aria-label="Checkbox for following text input"
                                       defaultChecked={this.props.questionToModify.answers[index].correct}
                                       onChange={this.handleChange} />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" name={"answer-" + index} id={"answer-" + index}
                               defaultValue={this.props.questionToModify.answers[index].label}
                               onChange={this.handleChange} placeholder="Réponse" />
                    </InputGroup>
                </FormGroup>
            );
        });
    }

    handleChange(event){
        let regex = "\\d+";
        let formFieldUpdated = this.state.formField;

        if(event.target.name.includes("answer")){
            let indexToModify = event.target.name.match(regex)[0];
            if(this.state.formField.answers === []){
                this.setState({formField: {answers: this.props.questionToModify.answers}});
            }
            if(event.target.type === 'checkbox'){
                formFieldUpdated.answers[indexToModify].correct = event.target.checked;
                this.setState({formField: formFieldUpdated});
            } else {
                formFieldUpdated.answers[indexToModify].label = event.target.value;
                this.setState({formField: formFieldUpdated});
            }
        } else if(event.target.type === 'checkbox'){
            formFieldUpdated[event.target.name] = event.target.checked;
            this.setState({formField: formFieldUpdated});
        } else {
            formFieldUpdated[event.target.name] = event.target.value;
            this.setState({formField: formFieldUpdated});
        };
    }

    handleSubmit(event) {
        let config = {
            headers: {"Authorization": "Bearer " + this.props.authtoken}
        };
        let self = this;
        if(this.state.formField.label){
            let labelToUpdate = {
                'label': this.state.formField.label
            };
            Axios.put(window.API_URL + "admin/questions/" + self.props.questionToModify.questionId, labelToUpdate, config)
                .then(() => {
                    console.log("Succesfully updated label");
                });
        };
        let updatedAnswers = this.state.formField.answers;
        Axios.put(window.API_URL + "admin/questions/" + self.props.questionToModify.questionId + "/answers", updatedAnswers, config)
            .then(() => {
                console.log("Succesfully updated answers");
                this.props.updateList();
            });
        this.props.toggleModifyQuestionModal();

    }

    render(){
        return(
            <Modal isOpen={this.props.modifyQuestionModal} toggle={() => this.props.toggleModifyQuestionModal(null)}>
                <ModalHeader>Modifier la question</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="questionId">Intitulé de la question</Label>
                            <Input type="text" name="label" id="questionId"
                                   onChange={this.handleChange} defaultValue={this.props.questionToModify.label} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="position">Position dans le quiz</Label>
                            <Input type="number" name="position" id="position" onChange={this.handleChange}
                                   defaultValue={this.props.questionToModify.position}/>
                        </FormGroup>
                        {/*<FormGroup check inline>*/}
                        {/*    <Label for="questionStatus">Active ?</Label>*/}
                        {/*    <Input type="checkbox" name="status" defaultValue={this.props.questionToModify.status}*/}
                        {/*           onChange={this.handleChange} id="questionStatus"/>*/}
                        {/*</FormGroup>*/}
                        <FormGroup>
                            <Label>Réponses (cocher la bonne réponse)</Label>
                            <Button onClick={this.addNewAnswerField}>+</Button>
                            {this.generateAnswerFields()}
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}>Valider</Button>
                    <Button color="warning" onClick={() => this.props.toggleModifyQuestionModal(null)}>Annuler</Button>
                </ModalFooter>
            </Modal>
        );
    }

    componentDidUpdate(prevProps) {
        if(this.props.questionToModify){
            const answers = this.props.questionToModify.answers;
            if (this.state.formField.answers !== answers) {
                if (answers) {
                    let updateState = this.state;
                    updateState.formField.answers = answers;
                    this.setState(updateState);
                }
            }
        }
    }
}

export {ModifyQuestionModal};