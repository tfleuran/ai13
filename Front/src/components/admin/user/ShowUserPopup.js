import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table, Row} from "reactstrap";

class ShowUserPopup extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <Modal isOpen={this.props.showUserModal} toggle={this.toggleShowUserModal}>
                <ModalHeader>Affichage utilisateur</ModalHeader>
                <ModalBody>
                    <Table>
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Entreprise</th>
                            <th>Telephone</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>{this.props.userToShow.name}</th>
                            <th>{this.props.userToShow.email}</th>
                            <th>{this.props.userToShow.company}</th>
                            <th>{this.props.userToShow.phoneNumber}</th>
                        </tr>
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="warning" onClick={this.props.toggleShowUserModal}>Fermer</Button>
                </ModalFooter>
            </Modal>

        )
    }
}

export {ShowUserPopup};