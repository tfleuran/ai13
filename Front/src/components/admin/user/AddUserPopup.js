import React, {Component} from 'react';
import Axios from "axios";
import {Button, Col, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";

class AddUserPopup extends Component {
    constructor(props) {
        super(props);
        this.state =  {
            formField : {
                admin: false,
                company: "",
                email: "",
                name: "",
                password: "",
                phoneNumber: "",
                status: false
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event){
        let formFieldUpdated = this.state.formField;
        if(event.target.type === 'checkbox'){
            formFieldUpdated[event.target.name] = event.target.checked;
            this.setState({formField: formFieldUpdated});
        } else {
            formFieldUpdated[event.target.name] = event.target.value;
            this.setState({formField: formFieldUpdated});
        };
    }

    handleSubmit(event){
        let config = {
            headers : {"Authorization": "Bearer " + this.props.authtoken}
        };
        let userData = {
            'admin': this.state.formField.admin,
            'company': this.state.formField.company,
            'email': this.state.formField.email,
            'name': this.state.formField.name,
            'phoneNumber': this.state.formField.phoneNumber,
            'password': this.state.formField.password,
            'status': this.state.formField.status
        };
        let self = this;
        Axios.post(window.API_URL + "admin/users" , userData, config )
            .then(response => {
                console.log("Succesfully added User: " + response.data);
                self.setState({formField: {
                        admin: false,
                        company: "",
                        email: "",
                        name: "",
                        password: "",
                        phoneNumber: "",
                        status: false
                    }})
                self.props.toggleAddUserModal();
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
    }

    render(){
        return(
            <Modal isOpen={this.props.addUserModal} toggle={this.toggleAddUserModal}>
                <ModalHeader>Ajouter un utilisateur</ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="userID">Identifiant (mail)</Label>
                                    <Input type="email" name="email" id="userID"
                                           onChange={this.handleChange} placeholder="Adresse mail" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="userPassword">Password</Label>
                                    <Input type="password" name="password" id="userPassword"
                                           onChange={this.handleChange} placeholder="Mot de passe" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label for="userName">Nom</Label>
                            <Input type="text" name="name" id="userName" onChange={this.handleChange}
                                   placeholder="Dupond"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="userCompany">Societé</Label>
                            <Input type="text" name="company" id="userCompany"
                                   onChange={this.handleChange} placeholder="ADP :-)"/>
                        </FormGroup>
                        <Row form>
                            <Col md={4}>
                                <FormGroup>
                                    <Label for="userPhoneNumber">Téléphone</Label>
                                    <Input type="text" name="phoneNumber"
                                           onChange={this.handleChange} id="userPhoneNumber"/>
                                </FormGroup>
                            </Col>
                        </Row>
                            <FormGroup check inline>
                                <Label for="userRole">Administrateur ?</Label>
                                <Input type="checkbox" name="admin"
                                       onChange={this.handleChange} id="userRole"/>
                            </FormGroup>
                            <FormGroup check inline>
                                <Label for="userStatus">Actif ?</Label>
                                <Input type="checkbox" name="status"
                                       onChange={this.handleChange} id="userStatus"/>
                            </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}>Valider</Button>
                    <Button color="warning" onClick={this.props.toggleAddUserModal}>Annuler</Button>
                </ModalFooter>
            </Modal>

        )
    }
}
export {AddUserPopup};