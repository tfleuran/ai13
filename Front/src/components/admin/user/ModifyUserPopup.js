import React, {Component} from 'react';
import Axios from "axios";
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class ModifyUserPopup extends Component {
    constructor(props) {
        super(props);
        this.state =  {
            formField : {
                name: null,
                company: null,
                phoneNumber: null
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        let formFieldUpdated = this.state.formField;
        formFieldUpdated[event.target.name] = event.target.value;
        this.setState({formField: formFieldUpdated});
    }

    handleSubmit(event){
        let config = {
            headers : {"Authorization": "Bearer " + this.props.authtoken}
        };
        let userData = {
            'name': this.state.formField.name ? this.state.formField.name : this.props.userToModify.name,
            'company': this.state.formField.company ? this.state.formField.company : this.props.userToModify.company,
            'phoneNumber': this.state.formField.phoneNumber ? this.state.formField.phoneNumber : this.props.userToModify.phoneNumber
        };
        let self = this;
        Axios.put(window.API_URL + "admin/users/" + this.props.userToModify.userId , userData, config )
            .then(response => {
                console.log(response.data);
                console.log("Succesfully modified User: " + response.data.email);
                this.setState({
                    formField : {
                        name: null,
                        company: null,
                        phoneNumber: null
                    }
                });
                self.props.toggleModificationModal();
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
    }

    render(){
        return(
            <Modal isOpen={this.props.modifyUserModal} toggle={() => this.props.toggleModificationModal(null)}>
                <ModalHeader>Ajouter un utilisateur</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="userName">Nom</Label>
                            <Input type="text" name="name" id="userName" onChange={this.handleChange}
                                   defaultValue={this.props.userToModify.name}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="userCompany">Societé</Label>
                            <Input type="text" name="company" id="userCompany"
                                   onChange={this.handleChange} defaultValue={this.props.userToModify.company}/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="userPhoneNumber">Téléphone</Label>
                            <Input type="text" name="phoneNumber"
                                   onChange={this.handleChange} id="userPhoneNumber"
                                   defaultValue={this.props.userToModify.phoneNumber}/>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}>Valider</Button>
                    <Button color="warning" onClick={() => this.props.toggleModificationModal(null)}>Annuler</Button>
                </ModalFooter>
            </Modal>

        )
    }
}
export {ModifyUserPopup};