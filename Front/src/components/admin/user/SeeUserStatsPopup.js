import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from 'reactstrap';
import React from "react";

class SeeUserStatsPopup extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            headers: {
                quiz: "Questionnaire",
                bestScore: "Meilleur score",
                bestTime: "Meilleur temps"
            },
            userStats: []
        }

    }

    generateHeaders(){
        const {quiz, bestScore, bestTime} = this.state.headers;
        return (
            <tr key="listUsersHeader">
                <th>{quiz}</th>
                <th>{bestScore}</th>
                <th>{bestTime}</th>
            </tr>
        );
    }

    generateUserStats(){
        this.state.userStats.map((entry,index) => {
            const {quiz, bestScore, bestTime} = entry;
            return(
                <tr key={index}>
                    <td>{quiz}</td>
                    <td>{bestScore}</td>
                    <td>{bestTime}</td>
                </tr>
            );
        })
    }

    render(){
        return(
            <Modal isOpen={this.props.seeUserStats} toggle={this.props.toggleUserStatsPopup}>
                <ModalHeader>Statistiques de l'utilisateur:  </ModalHeader>
                <ModalBody>
                    <Table className="headerFixAlign" bordered>
                        <thead>
                            {this.generateHeaders()}
                        </thead>
                        <tbody id="listBody">
                            {this.generateUserStats()}
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="warning" onClick={this.props.toggleUserStatsPopup}>Retour</Button>
                </ModalFooter>
            </Modal>
        );

    }
}

export {SeeUserStatsPopup};