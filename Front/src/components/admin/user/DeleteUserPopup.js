import React, {Component} from 'react';
import Axios from "axios";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class DeleteUserPopup extends Component {
    constructor(props) {
        super(props);
        this.state =  {
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        let formFieldUpdated = this.state.formField;
        formFieldUpdated[event.target.name] = event.target.value;
        this.setState({formField: formFieldUpdated});
    }

    handleSubmit(event){
        let config = {
            headers : {"Authorization": "Bearer " + this.props.authtoken}
        };
        let self = this;
        self.props.toggleUserDeletionModal();
        /*  TODO    Pour supprimer un user c'est ok mais... je viens de penser que l'on ne les supprime pas, on va juste
            TODO    les désactiver nope ? bref dans l'API pour le moment il n'y a pas de delete, donc commentaires à
            TODO    supprimer si cela change
        Axios.delete("http://localhost:5000/admin/users/" + this.props.userToDelete.userId , config )
            .then(response => {
                console.log(response.data);
                console.log("Succesfully deleted User: " + response.data.email);
                //self.props.toggleUserDeletionModal();
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
            */
        // Desactivation du user
        Axios.patch(window.API_URL + "admin/users/" + this.props.userToDelete.userId + "/status", { 'status': false}, config )
            .then(response => {
                console.log(response.data);
                console.log("Succesfully desactivate the User: " + response.data.email);
                //self.props.toggleUserDeletionModal();
                self.props.updateList();
            })
            .catch(error => {
                console.error(error);
            });
    }

    render(){
        return(
            <Modal isOpen={this.props.deleteUserModal} toggle={() => this.props.toggleUserDeletionModal(null)}>
                <ModalHeader>Supprimer un utilisateur</ModalHeader>
                <ModalBody>
                    <p>Vous êtes sur le point de desactiver un utilisateur, êtes-vous sûr de vouloir faire cela ?</p>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={(e) => {e.preventDefault(); this.handleSubmit(e)}}>Valider</Button>
                    <Button color="warning" onClick={() => this.props.toggleUserDeletionModal(null)}>Annuler</Button>
                </ModalFooter>
            </Modal>

        )
    }
}
export {DeleteUserPopup};