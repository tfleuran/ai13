Select 1;
DROP TABLE IF EXISTS course_answer;
DROP TABLE IF EXISTS course CASCADE ;
DROP TABLE IF EXISTS users CASCADE ;
DROP TABLE IF EXISTS quiz CASCADE ;
DROP TABLE IF EXISTS answer CASCADE ;
DROP TABLE IF EXISTS question CASCADE ;
DROP TABLE IF EXISTS subject CASCADE;
DROP SEQUENCE IF EXISTS hibernate_sequence CASCADE ;

CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START 1;

CREATE TABLE users(
    email varchar(50) PRIMARY KEY,
    password text not null,
    name varchar(50) not null,
    company varchar(50),
    phone_number varchar(12),
    status boolean not null ,
    admin boolean not null ,
    creation_date timestamp
);

CREATE TABLE subject(
    label varchar(50) PRIMARY KEY
);

CREATE TABLE quiz(
    id int PRIMARY KEY,
    title varchar (50) not null,
    description varchar (50),
    status boolean not null ,
    position int,
    id_subject varchar REFERENCES subject(label)
);

CREATE TABLE question(
    id int PRIMARY KEY,
    label varchar (50) not null,
    status boolean not null,
    position int,
    id_quiz int REFERENCES quiz(id)
);

CREATE TABLE answer(
    id int PRIMARY KEY,
    label varchar (50) not null,
    description varchar (50),
    status boolean not null ,
    is_correct boolean not null,
    position int,
    id_question int REFERENCES question(id)
);

CREATE TABLE course(
    id_course bigint PRIMARY KEY,
    score integer not null,
    duration bigint,
    creation_date timestamp,
    email varchar(50) not null,
    id_quiz bigint not null,
    UNIQUE (email, id_quiz), -- TODO : Check if a user can retake a quiz
    FOREIGN KEY (email) REFERENCES users(email),
    FOREIGN KEY (id_quiz) REFERENCES quiz(id)
);

CREATE TABLE course_answer(
    id_course bigint,
    id_answer bigint,
    PRIMARY KEY (id_course, id_answer),
    FOREIGN KEY (id_course) REFERENCES course(id_course),
    FOREIGN KEY(id_answer) REFERENCES answer(id)
);

--- INSERT
-- admin@utc.fr , password = admin
INSERT INTO public.users (email, password, name, company, phone_number, status, admin, creation_date) VALUES ('admin@utc.fr', '$2a$10$Q/cfhsgDowFKvZ9QfPWFQO510PyOpMlmTlyJrK400lS.hg7MyIiJe', 'string', 'string', 'string', true, true, '2019-11-29 16:35:48.479000');
-- user@utc.fr, password = user
INSERT INTO public.users
(email, "password", name, company, phone_number, status, "admin", creation_date)
VALUES('user@utc.fr', '$2a$10$639zr8mPhXOGmnmZbu8rW.riKT1ouau9IsnWvs12msX6fxjvPAKIO', '', '', '', true, false, '2020-01-07 10:09:32.824');
INSERT INTO public.subject (label) VALUES ('WebServices');
INSERT INTO public.quiz
(id, title, description, status, "position", id_subject)
VALUES(1, 'WebServices 101', NULL, true, NULL, 'WebServices');
INSERT INTO public.question
(id, "label", status, "position", id_quiz)
VALUES(2, 'Hello World 1', true, 1, 1);
INSERT INTO public.question
(id, "label", status, "position", id_quiz)
VALUES(5, 'Hello World 2', true, 2, 1);
INSERT INTO public.answer
(id, "label", description, status, is_correct, "position", id_question)
VALUES(3, 'La mauvaise réponse à la question 1', NULL, true, false, 0, 2);
INSERT INTO public.answer
(id, "label", description, status, is_correct, "position", id_question)
VALUES(4, 'La bonne réponse à la question 1', NULL, true, true, 0, 2);
INSERT INTO public.answer
(id, "label", description, status, is_correct, "position", id_question)
VALUES(6, 'La mauvaise réponse à la question 2', NULL, true, false, 0, 5);
INSERT INTO public.answer
(id, "label", description, status, is_correct, "position", id_question)
VALUES(7, 'La bonne réponse à la question 2', NULL, true, true, 0, 5);
