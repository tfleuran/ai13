package com.ai13.groupe2.backend.controller;

import com.ai13.groupe2.backend.domain.dto.request.UserRequest;
import com.ai13.groupe2.backend.domain.dto.request.UserSecurityRequest;
import com.ai13.groupe2.backend.service.user.UserBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Api(tags = "Security")
public class SecurityController {

    private final UserBusiness userBusiness;

    @Autowired
    public SecurityController(UserBusiness userBusiness) {
        this.userBusiness = userBusiness;
    }

    //The following operation is only here to document login Spring Security Operation, and allowing its use in Swagger UI
    @ApiOperation(value="Login.")
    @PostMapping("/login")
    public void fakeLogin(@ApiParam("User") @RequestBody UserSecurityRequest user) {
        throw new IllegalStateException("This method shouldn't be called. It's implemented by Spring Security filters.");
    }


    // TODO: A désactiver quand un admin pourra créer des users
    @PostMapping("/signup")
    @ResponseBody
    public ResponseEntity<?> signUp(@ApiParam("User") @RequestBody UserRequest user)
    {
        Map<String, String> response = userBusiness.SignUp(user);

        if(response.containsKey("error")){
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

    }
}
