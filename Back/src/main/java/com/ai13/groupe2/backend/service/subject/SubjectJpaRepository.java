package com.ai13.groupe2.backend.service.subject;

import com.ai13.groupe2.backend.domain.dao.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface SubjectJpaRepository extends JpaRepository<Subject, String> {

    Subject findByLabel(String label);

}
