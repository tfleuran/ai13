package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Subject;

import java.util.List;
import java.util.stream.Collectors;

public class QuizExtendedResource {
    private Integer id;
    private String title;
    private Boolean status;
    private String description;
    private Subject subject;
    private List<QuestionExtendedResource> questions;
    private boolean activable;

    public QuizExtendedResource(Quiz quiz){
        this.id = quiz.getId();
        this.title = quiz.getTitle();
        this.description = quiz.getDescription();
        this.status = quiz.getStatus();
        this.subject = quiz.getSubject();
        this.questions = quiz.getQuestions().stream().map(QuestionExtendedResource::new).collect(Collectors.toList());
        this.activable = quiz.isActivable();
    }

    public String getTitle() {
        return title;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getSubject() {
        return subject.getLabel();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<QuestionExtendedResource> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionExtendedResource> questions) {
        this.questions = questions;
    }

    public void setActivable(boolean activable) {
        this.activable = activable;
    }

    public boolean isActivable() {
        return activable;
    }
}
