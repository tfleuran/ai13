package com.ai13.groupe2.backend.service.user;

import lombok.Getter;

@Getter
public enum UserSql {

    GET_ALL(
        "SELECT email, name, company, phone_number, admin, status, creation_date " +
                "FROM users"
    ),
    GET_ALL_BY_TERMS(
            "SELECT email, name, company, phone_number, admin, status, creation_date FROM users where 1=1\n" +
                    "and admin = :admin --booleans are initiazed at false by default\n" +
                    "and status = :status\n"
    );
    private String query;

    UserSql(String query) {
		this.query = query;
    }

    public String getQuery(){
        return query;
    }
}
