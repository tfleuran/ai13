package com.ai13.groupe2.backend.service.Course;

import com.ai13.groupe2.backend.domain.dao.Course;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResource;
//import com.ai13.groupe2.backend.domain.dto.resource.QuizResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CourseRepositoryImpl implements CourseRepository{

    private final CourseJpaRepository courseJpaRepository;
    @Autowired
    public CourseRepositoryImpl(CourseJpaRepository courseJpaRepository) {
        this.courseJpaRepository = courseJpaRepository;
    }

    @Override
    public Course save(Course course) {
        return courseJpaRepository.save(course);
    }

    @Override
    public List<CourseResource> getAllCourses(String username) {
//        List<CourseResource> courseResourceList = new ArrayList<>();
//        List<Course> courseList = courseJpaRepository.findAll();

//        for (Course course: courseList){
//            CourseResource courseResource = new CourseResource(course);
//            courseResourceList.add(courseResource);
//        }

        return courseJpaRepository.findAllByUserEmail(username).stream().map(CourseResource::new).collect(Collectors.toList());

    }

    @Override
    public Course findOneById(int id) {
        return courseJpaRepository.findOneById(id);
    }
}
