package com.ai13.groupe2.backend.service.subject;

import com.ai13.groupe2.backend.domain.dto.resource.SubjectResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SubjectBusinessImpl implements SubjectBusiness{

    private SubjectRepository subjectRepository;

    @Autowired
    public SubjectBusinessImpl(SubjectRepository subjectRepository){
        this.subjectRepository = subjectRepository;
    }

    @Override
    public List<SubjectResource> getAllSubjects(){
        return subjectRepository.getAllSubjects();
    }
}
