package com.ai13.groupe2.backend.service.question;

import com.ai13.groupe2.backend.domain.dto.request.AnswerRequest;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;

import java.util.List;

public interface QuestionBusiness {
    List<QuestionResource> getAllQuestions();

    List<QuestionResource> getActiveQuestions();

    QuestionResource putLabel(int id, String newLabel) throws ResourceNotFoundException, Exception;

    QuestionResource patchStatus(int id) throws ResourceNotFoundException, Exception;

    AnswerResource addAnswerToQuestion(int idQuestion, AnswerRequest answerRequest);

    public List<AnswerResource> getAllAnswersForQuestion(int question_id);

    QuestionExtendedResource putAnswerList(int id, List<AnswerRequest> answerRequests);
}
