package com.ai13.groupe2.backend.service.subject;

import com.ai13.groupe2.backend.domain.dto.request.SubjectRequest;
import com.ai13.groupe2.backend.domain.dto.resource.SubjectResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

public interface SubjectBusiness {

    List<SubjectResource> getAllSubjects();

//    List<SubjectResource> searchSubjectsByTerms(SubjectRequest subjectRequest);
}
