package com.ai13.groupe2.backend.controller;

import com.ai13.groupe2.backend.domain.dto.request.CourseRequest;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResource;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResultExtendedResource;
import com.ai13.groupe2.backend.service.Course.CourseBuisness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "Courses")
public class CourseController {
    private final CourseBuisness courseBuisness;

    @Autowired
    public CourseController(CourseBuisness courseBuisness) {
        this.courseBuisness = courseBuisness;
    }


    @PreAuthorize("hasRole('ROLE_PRIVATE')")
    @PostMapping(value = "/private/courses")
    @ApiOperation(value = "Create a Course",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> postCourse(@ApiParam("Course") @RequestBody CourseRequest courseRequest, Principal principal){
        Map<String, String> response = courseBuisness.addCourse(courseRequest, principal.getName());

        if(response.containsKey("error")){
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
    }

    @PreAuthorize("hasRole('ROLE_PRIVATE')")
    @GetMapping(value = "/private/courses/results")
    @ApiOperation(value = "Get all Courses as user",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<CourseResource> getAllCourses(Principal principal){
        return courseBuisness.getAllCourses(principal.getName());
    }


    @PreAuthorize("hasRole('ROLE_PRIVATE')")
    @GetMapping(value = "/private/courses/{id}")
    @ApiOperation(value="Get all results for a quiz for a user (ie. a Course)",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> getQuizWithActiveQuestions(@PathVariable int id, Principal principal) {
        try {
            CourseResultExtendedResource courseResultExtendedResource = courseBuisness.getCourseResultForUser(id, principal.getName());
            return new ResponseEntity<>(courseResultExtendedResource, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }

    }


}
