package com.ai13.groupe2.backend.domain.dto.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(value="CourseRequest", description="Course Model DTO for Requests")
public class CourseRequest {
    @ApiModelProperty(required = true)
    private int score;
    private long durationInMilli;
//    private String user;
    private int quiz;
    private List<Integer> answers;
//
//    public String getUser() {
//        return user;
//    }
//
//    public void setUser(String user) {
//        this.user = user;
//    }

    public int getQuiz() {
        return quiz;
    }


    public void setQuiz(int quiz) {
        this.quiz = quiz;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getDuration() {
        return durationInMilli;
    }

    public void setDuration(long durationInMilli) {
        this.durationInMilli = durationInMilli;
    }

//    public LocalDateTime getCreationDate() {
//        return creationDate;
//    }

//    public void setCreationDate(LocalDateTime creationDate) {
//        this.creationDate = creationDate;
//    }

    public List<Integer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Integer> answers) {
        this.answers = answers;
    }
}
