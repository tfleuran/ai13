package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Course;
import com.ai13.groupe2.backend.domain.dao.Quiz;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.time.Duration;

public class CourseResource {
    private int id;
    private int score;
    private Duration duration;
    private Timestamp creationDate;
    private QuizResource quizResource;

    public CourseResource() {
    }

    public CourseResource(int id, int score, Duration duration, Timestamp creationDate) {
        this.id = id;
        this.score = score;
        this.duration = duration;
        this.creationDate = creationDate;
    }

    public CourseResource(Course course) {
        this.id = course.getId();
        this.score = course.getScore();
        this.duration = course.getDuration();
        this.creationDate = course.getCreationDate();
        this.quizResource = new QuizResource(course.getQuiz());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getDuration() {
        return duration.toMillis();
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public QuizResource getQuizResource() {
        return quizResource;
    }

    public void setQuizResource(QuizResource quizResource) {
        this.quizResource = quizResource;
    }
}
