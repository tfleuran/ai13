package com.ai13.groupe2.backend.domain.dto.resource;


import com.ai13.groupe2.backend.domain.dao.Subject;

import java.util.Date;

//@Data
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
public class SubjectResource {

    private String label;

    public SubjectResource(String label
    ) {
        this.label = label;
    }

    public SubjectResource(Subject subject
    ) {
        this.label = subject.getLabel();
    }

    public String getLabel() {
        return label;
    }
}
