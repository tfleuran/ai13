package com.ai13.groupe2.backend.service.Course;

import com.ai13.groupe2.backend.domain.dao.Course;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResource;

import java.util.List;

public interface CourseRepository {
    public Course save(Course course);

    List<CourseResource> getAllCourses(String username);

    Course findOneById(int id);
}
