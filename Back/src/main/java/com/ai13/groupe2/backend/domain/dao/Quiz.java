package com.ai13.groupe2.backend.domain.dao;

import com.ai13.groupe2.backend.domain.dto.resource.QuizResource;
import lombok.Data;
import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Data
@Entity
@Table(name = "quiz")
@SqlResultSetMapping(name = "AggregateListQuiz", classes = {
        @ConstructorResult(targetClass = QuizResource.class, columns = {
                @ColumnResult(name = "id", type = Integer.class),
                @ColumnResult(name = "title", type = String.class),
                @ColumnResult(name = "status", type = Boolean.class),
                @ColumnResult(name = "description", type = String.class),
                @ColumnResult(name = "subject", type = Subject.class)
        })
})
public class Quiz {

    @Id
    @Column (name = "id", unique = true, nullable = false)
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;

    @Column (name = "title", unique = true, nullable = false)
    private String title;

    @Column (name = "description")
    private String description;

    @Column (name = "status", nullable = false)
    private Boolean status;

    @ManyToOne
    @JoinColumn(name="id_subject")
    private Subject subject;

    @OneToMany(mappedBy = "quiz", fetch = FetchType.LAZY)
    private Collection<Question> questions;

    public Quiz(){
    }


    public Quiz(String title, String description, Boolean status, Subject subject) throws Exception {
        if (title==null || status==null) throw new Exception("Missing mandatory fields to create Form");
        this.title = title;
        this.description = description;
        this.status = status;
        this.subject = subject;
    }

    public boolean isActivable(){
        List<Question> activeQuestions = questions.stream().filter(Question::getStatus).collect(Collectors.toList());
        return activeQuestions.size() > 0;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Collection<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }
}
