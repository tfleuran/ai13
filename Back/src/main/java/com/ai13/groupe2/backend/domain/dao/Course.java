package com.ai13.groupe2.backend.domain.dao;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name ="course")
public class Course {

    @Id
    @Column(name = "id_course", unique = true, nullable = false)
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "score", nullable = false)
    private int score;

    @Column(name = "duration", nullable = false)
    private Duration duration;

    @Column(name = "creation_date", nullable = false)
    private Timestamp creationDate;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name="course_answer", joinColumns=@JoinColumn(name="id_course"),
            inverseJoinColumns=@JoinColumn(name="id_answer"))
    private Set<Answer> answers = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_quiz")
    private Quiz quiz;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email")
    private User user;

    public Course(){}

    public Course(int score, Duration duration, Set<Answer> answers,
                  Quiz quiz, User user) {
        this.score = score;
        this.duration = duration;
        this.creationDate = Timestamp.valueOf(LocalDateTime.now());
        this.answers = answers;
        this.quiz = quiz;
        this.user = user;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public String toSring(){
        return "id: " + this.getId() + " score: " + this.getScore() + " duration: " + this.getDuration();
    }
}
