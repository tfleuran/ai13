package com.ai13.groupe2.backend.exception;

public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(String errorMessage){
        super(errorMessage);
    }
}
