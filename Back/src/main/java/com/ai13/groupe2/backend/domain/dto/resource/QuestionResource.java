package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Question;

public class QuestionResource {
    private int id;
    private String label;
    private int position;
    private boolean status;
    private int id_quiz;

    public QuestionResource(Integer id, String label, Integer position, Boolean status) {
        this.id = id;
        this.label = label;
        this.position = position;
        this.status = status;
    }

    public QuestionResource(Question question){
        this.id = question.getId();
        this.label = question.getLabel();
        this.position = question.getPosition();
        this.status = question.getStatus();
    }
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

