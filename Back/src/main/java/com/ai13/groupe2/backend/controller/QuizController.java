package com.ai13.groupe2.backend.controller;

import com.ai13.groupe2.backend.domain.dto.request.QuizRequest;
import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import com.ai13.groupe2.backend.service.quiz.QuizBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "Quizzes")
public class QuizController {
    private final QuizBusiness quizBusiness;

    @Autowired
    public QuizController(QuizBusiness quizBusiness) {
        this.quizBusiness = quizBusiness;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/quizzes")
    @ApiOperation(value="Get All Quizzes",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<QuizResource> getAllQuizzes() {
        return quizBusiness.getAllQuizzes();
    }

    @PreAuthorize("hasRole('ROLE_PRIVATE')")
    @GetMapping(value = "/private/quizzes/actif")
    @ApiOperation(value="Get All Actif Quizzes",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<QuizResource> getAllActifQuizzes(){
        return quizBusiness.getAllActifQuizzes();
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping(value = "/admin/quizzes/{id}/status")
    @ApiOperation(value="Change the status of a Quiz",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> patchStatus(@PathVariable int id){
        try{
            QuizResource quizUpdated = quizBusiness.patchStatus(id);
            return new ResponseEntity<>(quizUpdated, HttpStatus.OK);
        }catch (ResourceNotFoundException e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/quizzes/{id}/questions")
    @ApiOperation(value="get All questions for a quiz",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<QuestionExtendedResource> getAllQuestionsForQuiz(@PathVariable int id) {
        return quizBusiness.getAllQuestionsForQuiz(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/admin/quizzes")
    @ApiOperation(value="Create Quiz as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> postQuiz(@ApiParam("Quiz") @RequestBody QuizRequest quiz)
    {
        Map<String, String> response = quizBusiness.addQuiz(quiz);

        if(response.containsKey("error")){
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/admin/quizzes/{id}")
    @ApiOperation(value = "Add question to Quiz",
            authorizations = {
                    @Authorization(value = "Bearer")
            })
    public ResponseEntity<?> addQuestionToQuiz(@PathVariable int id, @RequestBody QuestionRequest questionRequest){
        QuestionResource questionResource = quizBusiness.addQuestionToQuiz(id, questionRequest);

        if (questionResource == null){
            return new ResponseEntity<>(questionResource, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(questionResource, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_PRIVATE')")
    @GetMapping(value = "/private/quizzes/{id}")
    @ApiOperation(value="get quiz with all active questions for it",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public QuizExtendedResource getQuizWithActiveQuestions(@PathVariable int id) {
        return quizBusiness.getQuizWithActiveQuestions(id);
    }

    @PreAuthorize("hasRole('ROLE_PRIVATE')")
    @GetMapping(value = "/private/quizzes")
    @ApiOperation(value="Get available quizzes for the connected user",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<QuizResource> getAvailableQuizzes(Principal principal) {
        System.out.println(principal.getName());
        return quizBusiness.getAvailableQuizzes(principal.getName());
    }
}
