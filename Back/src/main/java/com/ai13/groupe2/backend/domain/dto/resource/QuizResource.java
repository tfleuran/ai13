package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Subject;

public class QuizResource {
    private Integer id;
    private String title;
    private Boolean status;
    private String description;
    private Subject subject;
    private boolean activable;

    public QuizResource(Integer id, String title, Boolean status, String description, Subject subject) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.description = description;
        this.subject = subject;
    }

    public QuizResource(Integer id, String title, Boolean status, String description, String subject) throws Exception {
        this.id = id;
        this.title = title;
        this.status = status;
        this.description = description;
        this.subject = new Subject(subject);
    }

    public QuizResource(Quiz quiz){
        this.id = quiz.getId();
        this.title = quiz.getTitle();
        this.description = quiz.getDescription();
        this.status = quiz.getStatus();
        this.subject = quiz.getSubject();
        this.activable = quiz.isActivable();

    }

    public String getTitle() {
        return title;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getSubject() {
        return subject.getLabel();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setActivable(boolean activable) {
        this.activable = activable;
    }

    public boolean isActivable() {
        return activable;
    }
}
