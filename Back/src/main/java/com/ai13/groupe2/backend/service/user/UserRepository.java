package com.ai13.groupe2.backend.service.user;

import com.ai13.groupe2.backend.domain.dao.User;
import com.ai13.groupe2.backend.domain.dto.request.UserRequest;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;

import java.util.List;

public interface UserRepository {

    List<UserResource> getAllUsers();

    List<UserResource> searchUsersByTerms(UserRequest userRequest);

    List<String> getSearchElementsList(String column, String value);

    User findByEmail(String email);

    void AddUser(User user);
}
