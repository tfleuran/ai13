package com.ai13.groupe2.backend.service.user;

import com.ai13.groupe2.backend.domain.dto.request.UserRequest;
import com.ai13.groupe2.backend.domain.dto.request.UserUpdateRequest;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

public interface UserBusiness {

    List<UserResource> getAllUsers();

    List<UserResource> searchUsersByTerms(UserRequest userRequest);

    List<String> getSearchElementsList(String column, String value);

    UserResource getAdminUser(String email);

    Map<String, String> SignUp(UserRequest user);

    UserResource patchStatus(String email) throws ResourceNotFoundException;

    UserResource putUser(String email, UserUpdateRequest userInfo) throws ResourceNotFoundException, Exception;
}
