package com.ai13.groupe2.backend.service.question;

import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface QuestionJpaRepository extends JpaRepository<Question, String> {
    Question findById(int id);

    Collection<Question> findAllByQuiz(Quiz id);

    int countByQuiz_IdAndStatus(int quiz, boolean status);

}
