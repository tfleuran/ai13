package com.ai13.groupe2.backend.service.question;

import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dto.request.AnswerRequest;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;

import java.util.List;

public interface QuestionRepository {
    List<QuestionResource> getAllQuestions();
    List<QuestionResource> getActiveQuestions();
    Question getQuestionById(int id);
    void addQuestion(Question question);
    void addAnswerToQuestion(int idQuestion, AnswerRequest answerRequest);

    int countByQuiz_IdAndStatus(int quiz, boolean status);
    List<Integer> getAllQuestionIdByQuiz(int form);
    Question save(Question question);

    public List<AnswerResource> getAllAnswersForQuestion(int question_id);
}
