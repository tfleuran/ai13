package com.ai13.groupe2.backend.controller;


import com.ai13.groupe2.backend.domain.dto.request.UserRequest;
import com.ai13.groupe2.backend.domain.dto.request.UserUpdateRequest;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import com.ai13.groupe2.backend.service.user.UserBusiness;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Api(tags = "Users"
)
public class UserController {

    private final UserBusiness userBusiness;

    @Autowired
    public UserController(UserBusiness userBusiness) {
        this.userBusiness = userBusiness;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/users/all")
    @ApiOperation(value="Get All Users",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<UserResource> getAllUsers() {
        return userBusiness.getAllUsers();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/admin/users/search")
    @ApiOperation(value="Get All Users corresponding exactly to provided criterias",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<UserResource> searchUsersByTerms(@ApiParam("User") @RequestBody UserRequest user) {
        return userBusiness.searchUsersByTerms(user);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/users/search/criteria")
    @ApiOperation(value="Get Values for a criteria, according to first 3 characters provided",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Wrong Column Name")
    })
    public ResponseEntity<?> getSearchElementsList(@ApiParam(value = "Either email, name, company or phone_number", required = true)
                                                  @RequestParam String column, @RequestParam(required = false, defaultValue = "") String value) {

        List<String> authorizedColumnNames = Arrays.asList("email", "name", "company", "phone_number");
        return authorizedColumnNames.contains(column)?
                new ResponseEntity<>(userBusiness.getSearchElementsList(column, value), HttpStatus.OK) :
                new ResponseEntity<>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/users/{email}")
    @ApiOperation(value="Get Single User as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public UserResource getAdminUser(@PathVariable String email){
        return userBusiness.getAdminUser(email);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/admin/users")
    @ApiOperation(value="Create User as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> postUser(@ApiParam("User") @RequestBody UserRequest user)
    {
        Map<String, String> response = userBusiness.SignUp(user);

        if(response.containsKey("error")){
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping(value = "/admin/users/{email}/status")
    @ApiOperation(value="Change status of a User as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> patchStatus(@PathVariable String email){
        try{
            UserResource user = userBusiness.patchStatus(email);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch (ResourceNotFoundException e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/admin/users/{email}")
    @ApiOperation(value="Update User as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> putUser(@PathVariable String email, @ApiParam("User") @RequestBody UserUpdateRequest userInfo){
        try{
            UserResource userUpdated = userBusiness.putUser(email, userInfo);
            return new ResponseEntity<>(userUpdated, HttpStatus.OK);
        }catch (ResourceNotFoundException e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
