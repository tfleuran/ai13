package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Question;

public class CourseResultQuestionResource {
    private String question;
    private String right_answer;
    private String user_answer;

    CourseResultQuestionResource(){}

    public CourseResultQuestionResource(String question, String right_answer, String user_answer){
        this.question = question;
        this.right_answer = right_answer;
        this.user_answer = user_answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRight_answer() {
        return right_answer;
    }

    public void setRight_answer(String right_answer) {
        this.right_answer = right_answer;
    }

    public String getUser_answer() {
        return user_answer;
    }

    public void setUser_answer(String user_answer) {
        this.user_answer = user_answer;
    }
}
