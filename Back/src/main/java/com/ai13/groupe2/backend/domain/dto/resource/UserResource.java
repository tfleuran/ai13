package com.ai13.groupe2.backend.domain.dto.resource;


import com.ai13.groupe2.backend.domain.dao.User;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

//@Data
//@Builder
//@AllArgsConstructor
//@NoArgsConstructor
public class UserResource {

    private String email;
    private String name;
    private String company;
    private String phoneNumber;
    private boolean status;
    private boolean admin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date creationDate;


    public UserResource(String email, String name, String company, String phoneNumber, Boolean admin, Boolean status, Date creationDate
    ) {
        this.email = email;
        this.name = name;
        this.company = company;
        this.phoneNumber = phoneNumber;
        this.admin = admin;
        this.status = status;
        this.creationDate = creationDate;
    }

    public UserResource(User user
    ) {
        this.email = user.getEmail();
        this.name = user.getName();
        this.company = user.getCompany();
        this.phoneNumber = user.getPhoneNumber();
        this.admin = user.getAdmin();
        this.status = user.getStatus();
        this.creationDate = user.getCreationDate();
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getCompany() {
        return company;
    }

    public boolean isStatus() {
        return status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public boolean isAdmin() {
        return admin;
    }
}
