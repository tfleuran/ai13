package com.ai13.groupe2.backend.service.subject;

import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.domain.dto.resource.SubjectResource;
import org.springframework.context.annotation.Bean;

import java.util.List;

public interface SubjectRepository {
    List<SubjectResource> getAllSubjects();

    Subject save(Subject subject);
}
