package com.ai13.groupe2.backend.service.question;

import com.ai13.groupe2.backend.domain.dao.Answer;
import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dto.request.AnswerRequest;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.service.SharedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@SuppressWarnings("unchecked")
public class QuestionRepositoryImpl extends SharedRepository implements QuestionRepository {
    private static final String QUESTION_LIST_RESULT_MAPPING = "AggregateListQuestion";
    private static final String QUESTION_ID_LIST_BY_FORM_RESULT_MAPPING = "AggregateListIdQuestion";
    private final QuestionJpaRepository questionJpaRepository;

    @Autowired
    public QuestionRepositoryImpl(QuestionJpaRepository questionJpaRepository){
        this.questionJpaRepository = questionJpaRepository;
    }

    @Override
    public List<QuestionResource> getAllQuestions() {
        Query query = entityManager.createNativeQuery(QuestionSql.GET_ALL.getQuery(), QUESTION_LIST_RESULT_MAPPING);
        return query.getResultList();
    }

    @Override
    public List<QuestionResource> getActiveQuestions() {
        Query query = entityManager.createNativeQuery(QuestionSql.GET_ALL_ACTIVE.getQuery(), QUESTION_LIST_RESULT_MAPPING);
        return query.getResultList();
    }

    @Override
    public Question getQuestionById(int id){
        return questionJpaRepository.findById(id);
    }

    @Override
    public void addQuestion(Question question) {
    }

    @Override
    public void addAnswerToQuestion(int idQuestion, AnswerRequest answerRequest) {
    }

    @Override
    public int countByQuiz_IdAndStatus(int quiz, boolean status) {
        return questionJpaRepository.countByQuiz_IdAndStatus(quiz, status);
    }

    @Override
    public List<Integer> getAllQuestionIdByQuiz(int form) {
        Query query = entityManager.createNativeQuery(QuestionSql.GET_ALL_ACTIF_ID_BY_FORM.getQuery(), QUESTION_ID_LIST_BY_FORM_RESULT_MAPPING);
        query.setParameter("form", form);
        return query.getResultList();
    }

    @Override
    public Question save(Question question) {
        return questionJpaRepository.save(question);
    }

    @Override
    public List<AnswerResource> getAllAnswersForQuestion(int question_id) {
        Question question = questionJpaRepository.findById(question_id);
        List<Answer> answers = new ArrayList<>(question.getAnswers());
        List<AnswerResource> answerResources = answers.stream().map(AnswerResource::new).collect(Collectors.toList());
        return answerResources;
    }
}
