package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Answer;

public class AnswerResource {
    private int id;
    private String label;
    private int position;
    private Boolean status;
    private Boolean isCorrect;
    private int question;


    public AnswerResource(){}

    public AnswerResource(int id, String label, int position, Boolean status, Boolean isCorrect) {
        this.id = id;
        this.label = label;
        this.position = position;
        this.status = status;
        this.isCorrect = isCorrect;
    }

    public AnswerResource(Answer answer){
        this.id = answer.getId();
        this.label = answer.getLabel();
        this.isCorrect = answer.getCorrect();
        this.position = answer.getPosition();
        this.status = answer.getStatus();
//        this.question = answer.getQuestion() != null ? answer.getQuestion().getId(): null;
        this.question = answer.getQuestion().getId();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }

    public int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
