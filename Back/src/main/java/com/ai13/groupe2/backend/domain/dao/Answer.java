package com.ai13.groupe2.backend.domain.dao;

import com.ai13.groupe2.backend.domain.dto.request.AnswerRequest;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "answer")
@SqlResultSetMapping(name = "AggregateListResponse", classes = {
        @ConstructorResult(targetClass = AnswerResource.class, columns = {
                @ColumnResult(name = "id", type = Integer.class),
                @ColumnResult(name = "label", type = String.class),
                @ColumnResult(name = "position", type = Integer.class),
                @ColumnResult(name = "status", type = Boolean.class),
                @ColumnResult(name = "is_correct", type = Boolean.class),
        })
})
public class Answer {
    @Id
    @Column(name = "id", unique = true, nullable = false, insertable = false, updatable = false)
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;

    @Column (name = "label", nullable = false)
    private String label;

    @Column (name = "position", nullable = false)
    private int position;

    @Column (name = "status", nullable = false)
    private Boolean status;

    @Column (name = "is_correct", nullable = false)
    private Boolean isCorrect;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "id_question", nullable = false)
    private Question question;

    public Answer(){
    }

    public Answer(String label, int position, Boolean status, Boolean isCorrect, Question question) {
        this.label = label;
        this.position = position;
        this.status = status;
        this.isCorrect = isCorrect;
        this.question = question;
    }

    public Answer(AnswerRequest answerRequest) {
        this.label = answerRequest.getLabel();
        this.isCorrect = answerRequest.getCorrect();
        this.position = answerRequest.getPosition();
        this.status = answerRequest.getStatus();
    }

    public void updateFromAnswerRequest(AnswerRequest answerRequest){
        this.label = answerRequest.getLabel();
        this.isCorrect = answerRequest.getCorrect();
        this.position = answerRequest.getPosition();
        this.status = answerRequest.getStatus();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
