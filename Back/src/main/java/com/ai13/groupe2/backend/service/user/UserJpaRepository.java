package com.ai13.groupe2.backend.service.user;

import com.ai13.groupe2.backend.domain.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserJpaRepository extends JpaRepository<User, String> {
    User findOneByEmail(String email);

}
