package com.ai13.groupe2.backend.service.Course;

import com.ai13.groupe2.backend.domain.dao.*;
import com.ai13.groupe2.backend.domain.dto.request.CourseRequest;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResource;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResultQuestionResource;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResultExtendedResource;
import com.ai13.groupe2.backend.service.answer.AnswerJpaRepository;
import com.ai13.groupe2.backend.service.quiz.QuizJpaRepository;
import com.ai13.groupe2.backend.service.user.UserJpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class CourseBuisnessImpl implements CourseBuisness{

    private final CourseRepository courseRepository;
    private final QuizJpaRepository quizJpaRepository;
    private final UserJpaRepository userJpaRepository;
    private final AnswerJpaRepository answerJpaRepository;

    public CourseBuisnessImpl(CourseRepository courseRepository, QuizJpaRepository quizJpaRepository,
                              UserJpaRepository userJpaRepository, AnswerJpaRepository answerJpaRepository) {
        this.courseRepository = courseRepository;
        this.quizJpaRepository = quizJpaRepository;
        this.userJpaRepository = userJpaRepository;
        this.answerJpaRepository = answerJpaRepository;
    }

    @Override
    public Map<String, String> addCourse(CourseRequest courseRequest, String username) {
        HashMap<String, String> response = new HashMap<>();

        try {
            Quiz quiz = quizJpaRepository.findById(courseRequest.getQuiz());
            User user = userJpaRepository.findOneByEmail(username);
            List<Answer> answerList = new ArrayList<>();
            for (int idAnswer : courseRequest.getAnswers()){
                answerList.add(answerJpaRepository.findById(idAnswer));
            }

            Set<Answer> answerSet = new HashSet<>(answerList);

            Course course = new Course(
                    courseRequest.getScore(),
                    Duration.ofMillis(courseRequest.getDuration()),
                    answerSet,
                    quiz,
                    user
            );

            courseRepository.save(course);
            response.put("id", course.toSring());
            return response;
        } catch (Exception e) {
            response.put("error", e.toString());
            return response;
        }
    }

    @Override
    public List<CourseResource> getAllCourses(String username) {
        return courseRepository.getAllCourses(username);
    }

    @Override
    public CourseResultExtendedResource getCourseResultForUser(int id_course, String username) throws Exception {
        User user = userJpaRepository.findOneByEmail(username);
        Course course = courseRepository.findOneById(id_course);
        if (course==null){
            throw new Exception("No course found");
        }
        if (!course.getUser().getEmail().equals(user.getEmail())){
            throw new Exception("User on Course is not the user requesting");
        }

        List<CourseResultQuestionResource> courseResultQuestionResourceList = new ArrayList<>();
        course.getAnswers().forEach((Answer user_answer) -> {
            Question question = user_answer.getQuestion();
            Answer right_answer = question.getAnswers().stream().filter(Answer::getCorrect).collect(Collectors.toList()).get(0);
            courseResultQuestionResourceList.add(new CourseResultQuestionResource(question.getLabel(), right_answer.getLabel(), user_answer.getLabel()));
        });

        return new CourseResultExtendedResource(course, courseResultQuestionResourceList);
    }
}
