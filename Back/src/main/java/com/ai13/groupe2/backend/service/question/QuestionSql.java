package com.ai13.groupe2.backend.service.question;

public enum QuestionSql {
    GET_ALL(
            "SELECT id, label, position, status " +
                    "FROM question"
    ),
    GET_ALL_ACTIVE(

            "SELECT id, label, position, status " +
                    "FROM question " +
                    "WHERE status is true"
    ),
    GET_ALL_ACTIF_ID_BY_FORM(
            "SELECT id " +
                    "FROM question " +
                    "WHERE id_quiz = :form " +
                    "AND status = true"
    );

    public String getQuery() {
        return query;
    }

    private String query;
    QuestionSql(String query){
        this.query = query;
    }
}
