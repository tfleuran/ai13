package com.ai13.groupe2.backend.service.user;

import com.ai13.groupe2.backend.domain.dao.User;
import com.ai13.groupe2.backend.domain.dto.request.UserRequest;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;
import com.ai13.groupe2.backend.service.SharedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class UserRepositoryImpl extends SharedRepository implements UserRepository {

    private static final String USER_LIST_RESULT_MAPPING = "AggregateList";
    private final UserJpaRepository userJpaRepository;

    @Autowired
    public UserRepositoryImpl(UserJpaRepository userJpaRepository) {
        this.userJpaRepository = userJpaRepository;
    }

    public List<UserResource> getAllUsers(){
        Query query = entityManager.createNativeQuery(UserSql.GET_ALL.getQuery(), USER_LIST_RESULT_MAPPING);
        return query.getResultList();
    }

    @Override
    public List<UserResource> searchUsersByTerms(UserRequest userRequest){
        StringBuilder whereClause = new StringBuilder();

        if(userRequest.getEmail()!= null) whereClause.append("and email = :email\n");
        if(userRequest.getName()!= null) whereClause.append("and name = :name\n");
        if(userRequest.getCompany()!= null) whereClause.append("and company = :company\n");
        if(userRequest.getPhoneNumber()!= null) whereClause.append("and phone_number = CAST(:phone_number AS text)");

        Query query = entityManager.createNativeQuery(UserSql.GET_ALL_BY_TERMS.getQuery().concat(whereClause.toString()), USER_LIST_RESULT_MAPPING);
        if(userRequest.getEmail()!= null) query.setParameter("email", userRequest.getEmail());
        if(userRequest.getName()!= null) query.setParameter("name", userRequest.getName());
        if(userRequest.getCompany()!= null) query.setParameter("company", userRequest.getCompany());
        if(userRequest.getPhoneNumber()!= null) query.setParameter("phone_number", userRequest.getPhoneNumber());
        query.setParameter("admin", userRequest.isAdmin());
        query.setParameter("status", userRequest.isStatus());

        return query.getResultList();
    }

    public List<String> getSearchElementsList(String column, String value){
        Query query = entityManager.createNativeQuery("SELECT DISTINCT " + column + " FROM users WHERE " + column + " LIKE :value ORDER BY " + column);
        query.setParameter("value", value.concat("%"));

        return query.getResultList();
    }

    public User findByEmail(String email){
        return userJpaRepository.findOneByEmail(email);
    }

    @Override
    public void AddUser(User user) {
        userJpaRepository.save(user);
    }
}
