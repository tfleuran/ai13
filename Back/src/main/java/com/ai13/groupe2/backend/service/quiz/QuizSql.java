package com.ai13.groupe2.backend.service.quiz;


import lombok.Getter;

@Getter
public enum QuizSql {

    GET_ALL(
            "SELECT id, title, status, description, id_subject as subject " +
                    "FROM quiz"
    ),
    GET_ALL_ACTIF(
            "SELECT id, title, description, status, position, id_subject as subject " +
            "FROM quiz " +
            "WHERE status is true"
    );

    private String query;

    QuizSql(String query) {
        this.query = query;
    }

    public String getQuery(){
        return query;
    }
}
