package com.ai13.groupe2.backend.service;

import com.ai13.groupe2.backend.domain.dao.BaseModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class SharedRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    protected BaseModel saveModel(BaseModel model){
        model = entityManager.merge(model);
        entityManager.flush();
        entityManager.detach(model);
        return model;
    }
}
