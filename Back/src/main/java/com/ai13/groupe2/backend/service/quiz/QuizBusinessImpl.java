package com.ai13.groupe2.backend.service.quiz;

import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.domain.dto.request.QuizRequest;
import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import com.ai13.groupe2.backend.service.answer.AnswerRepository;
import com.ai13.groupe2.backend.service.question.QuestionRepository;
import com.ai13.groupe2.backend.service.subject.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

@Service
@Transactional
public class QuizBusinessImpl implements QuizBusiness{

    private final QuizRepository quizRepository;
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final SubjectRepository subjectRepository;

    @Autowired
    public QuizBusinessImpl(QuizRepository quizRepository,
                            QuestionRepository questionRepository,
                            AnswerRepository answerRepository, SubjectRepository subjectRepository) {
        this.quizRepository = quizRepository;
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.subjectRepository = subjectRepository;
    }

    @Override
    public List<QuizResource> getAllQuizzes() {
        return quizRepository.getAllQuizzes();
    }

    @Override
    public QuizResource patchStatus(int id) throws ResourceNotFoundException, Exception {
        Quiz QuizToUpdate = quizRepository.findById(id);
        if(QuizToUpdate==null)
            throw new ResourceNotFoundException("Quiz not found");
        if(!QuizToUpdate.getStatus())
            if(questionRepository.countByQuiz_IdAndStatus(id, true) == 0)
                throw new Exception("This Quiz doesn't have any question");
        List<Integer> questions = questionRepository.getAllQuestionIdByQuiz(id);
        ListIterator<Integer> it = questions.listIterator();
        boolean questionWithoutCorrectAnswer = false;
        while(it.hasNext()&&!questionWithoutCorrectAnswer){
            if(answerRepository.countByQuestion_IdAndIsCorrectAndStatus(it.next(), true, true) == 0)
                questionWithoutCorrectAnswer = true;
        }
        if(questionWithoutCorrectAnswer)
            throw new Exception("This Quiz contains at least a question without valid answer");
        QuizToUpdate.setStatus(!QuizToUpdate.getStatus());
        return new QuizResource(QuizToUpdate);
    }

    @Override
    public QuestionResource addQuestionToQuiz(int id, QuestionRequest questionRequest) {
        Question questionToAdd = new Question(questionRequest);

        Quiz quiz = quizRepository.getOne(id);

        questionToAdd.setQuiz(quiz);

        Question questionAdded = questionRepository.save(questionToAdd);

        return new QuestionResource(questionAdded);
    }

    @Override
    public Map<String, String> addQuiz(QuizRequest quizDTO){
        HashMap<String, String> response = new HashMap<>();
        try{
            Subject subject = subjectRepository.save(new Subject(quizDTO.getSubject()));
            Quiz quiz = new Quiz(
                    quizDTO.getTitle(),
                    quizDTO.getDescription(),
                    false,
                    subject
            );
            quizRepository.addQuiz(quiz);
            response.put("id", quiz.getTitle());
            return response;
        }catch (Exception e){
            response.put("error", e.toString());
            return response;
        }
    }

    @Override
    public List<QuestionExtendedResource> getAllQuestionsForQuiz(int id) {
        return quizRepository.getAllQuestionsForQuiz(id);
    }

    @Override
    public QuizExtendedResource getQuizWithActiveQuestions(int id) {
        return quizRepository.getQuizWithActiveQuestions(id);
    }
    @Override
    public List<QuizResource> getAllActifQuizzes() {
        return quizRepository.getAllActifQuizzes();
    }

    @Override
    public List<QuizResource> getAvailableQuizzes(String user){
        return quizRepository.getAvailableQuizzes(user);
    }
}
