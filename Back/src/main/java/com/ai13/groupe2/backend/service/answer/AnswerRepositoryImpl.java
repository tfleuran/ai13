package com.ai13.groupe2.backend.service.answer;

import com.ai13.groupe2.backend.domain.dao.Answer;
import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import com.ai13.groupe2.backend.service.SharedRepository;
import com.ai13.groupe2.backend.service.question.QuestionJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
public class AnswerRepositoryImpl extends SharedRepository implements AnswerRepository {

    private final AnswerJpaRepository answerJpaRepository;
    private final QuestionJpaRepository questionJpaRepository;

    @Autowired
    public AnswerRepositoryImpl(AnswerJpaRepository answerJpaRepository, QuestionJpaRepository questionJpaRepository) {
        this.answerJpaRepository = answerJpaRepository;
        this.questionJpaRepository = questionJpaRepository;
    }

    @Override
    public List<Answer> findAllAnswersByIdQuestion(int idQuestion) {
        Question question = questionJpaRepository.findById(idQuestion);
        return new ArrayList<>(question.getAnswers());
    }

    @Override
    public Answer save(Answer answer) {
        return answerJpaRepository.save(answer);
    }

    @Override
    public int countByQuestion_IdAndIsCorrectAndStatus(int question, boolean isCorrect, boolean status) {
        return answerJpaRepository.countByQuestion_IdAndIsCorrectAndStatus(question, isCorrect, status);
    }
}
