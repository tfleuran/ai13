package com.ai13.groupe2.backend.security;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static com.ai13.groupe2.backend.security.SecurityConstants.HEADER_STRING;
import static com.ai13.groupe2.backend.security.SecurityConstants.TOKEN_PREFIX;
import static com.ai13.groupe2.backend.security.SecurityConstants.SECRET;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);

        if(header == null || !header.startsWith(TOKEN_PREFIX))
        {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authenticationToken = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request)
    {
        String token = request.getHeader(HEADER_STRING);
        if(token != null)
        {
            // parse the token
            String user = Jwts.parser()
                    .setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();

            //WARNING : Only works because we on only stock the Role on the token, and we have only one role
            //TODO: Tidy up this part
            Object rolesToken = Jwts.parser().setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody().get("scope");
            List<?> list = new ArrayList<>();
            if (rolesToken.getClass().isArray()) {
                list = Arrays.asList((Object[])rolesToken);
            } else if (rolesToken instanceof Collection) {
                list = new ArrayList<>((Collection<?>)rolesToken);
            }
            ArrayList<GrantedAuthority> roles = new ArrayList<>();
            if (list.size()==1) roles.add(new SimpleGrantedAuthority((String) ((LinkedHashMap) list.get(0)).get("authority")));
            //

            if(user != null)
            {
                return new UsernamePasswordAuthenticationToken(user, null, roles);
            }
            return null;
        }
        return null;
    }
}
