package com.ai13.groupe2.backend.service.user;

import com.ai13.groupe2.backend.domain.dao.User;
import com.ai13.groupe2.backend.domain.dto.request.UserRequest;
import com.ai13.groupe2.backend.domain.dto.request.UserUpdateRequest;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class UserBusinessImpl implements UserBusiness {

    private final UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    public UserBusinessImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder){
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;

    }

    @Override
    public List<UserResource> getAllUsers(){

        return userRepository.getAllUsers();
    }

    @Override
    public List<UserResource> searchUsersByTerms(UserRequest userRequest){
        return userRepository.searchUsersByTerms(userRequest);
    }

    @Override
    public List<String> getSearchElementsList(String column, String value){

        return userRepository.getSearchElementsList(column, value);
    }

    @Override
    public UserResource getAdminUser(String email){
        User userToRetrieve = userRepository.findByEmail(email);
        return userToRetrieve != null? new UserResource(userToRetrieve): null;
    }


    @Override
    public Map<String, String> SignUp(UserRequest userDTO){
        HashMap<String, String> response = new HashMap<>();
//        JSONObject jsonObject = new JSONObject(jsonToParse);

        try{
            if (userRepository.findByEmail(userDTO.getEmail()) != null){
                response.put("error", "User already exists");
                return response;
            }

            User user = new User(
                    userDTO.getEmail(),
                    bCryptPasswordEncoder.encode(userDTO.getPassword()),
                    userDTO.getName(),
                    userDTO.getCompany(),
                    userDTO.getPhoneNumber(),
                    userDTO.isStatus(),
                    userDTO.isAdmin()
            );
            userRepository.AddUser(user);
            response.put("id", user.getEmail());
            return response;
        }catch (Exception e){
            response.put("error", e.toString());
            return response;
        }
    }

    @Override
    public UserResource patchStatus(String email) throws ResourceNotFoundException{
        User userToUpdate = userRepository.findByEmail(email);
        if(userToUpdate==null)
            throw new ResourceNotFoundException("User not found");
        else {
            userToUpdate.switchStatus();
            return new UserResource(userToUpdate);
        }
    }

    @Override
    public UserResource putUser(String email, UserUpdateRequest userInfo) throws ResourceNotFoundException, Exception{
        User userToUpdate = userRepository.findByEmail(email);
        if(userToUpdate==null)
            throw new ResourceNotFoundException("User not found");
        else {
                userToUpdate.userUpdate(userInfo);
                return new UserResource(userToUpdate);
        }
    }

}
