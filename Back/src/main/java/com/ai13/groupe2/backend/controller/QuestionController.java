package com.ai13.groupe2.backend.controller;

import com.ai13.groupe2.backend.domain.dto.request.AnswerRequest;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import com.ai13.groupe2.backend.service.question.QuestionBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@Api(tags = "Questions")
public class QuestionController {

    private final QuestionBusiness questionBusiness;

    @Autowired
    public QuestionController(QuestionBusiness questionBusiness) {
        this.questionBusiness = questionBusiness;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/questions/all")
    @ApiOperation(value = "get all question as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<QuestionResource> getAllQuestions() {
        return questionBusiness.getAllQuestions();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/questions/all/active")
    @ApiOperation(value = "get all actif question as Admin",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<QuestionResource> getActiveQuestions(/*@RequestHeader String authorization*/) {
        return questionBusiness.getActiveQuestions();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/admin/questions/{id}")
    @ApiOperation(value="Update label of a Question as Admin",
            notes = "Update only label for now, id doesn't require the full body",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> putQuestion(@PathVariable int id, @RequestBody QuestionRequest questionRequest){
        try{
            QuestionResource questionUpdated = questionBusiness.putLabel(id, questionRequest.getLabel());
            return new ResponseEntity<>(questionUpdated, HttpStatus.OK);
        }catch (ResourceNotFoundException e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping(value = "/admin/questions/{id}/status")
    @ApiOperation(value="Change the status of a Question",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> patchStatus(@PathVariable int id){
        try{
            QuestionResource questionUpdated = questionBusiness.patchStatus(id);
            return new ResponseEntity<>(questionUpdated, HttpStatus.OK);
        }catch (ResourceNotFoundException e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/admin/questions/{question_id}/answers")
    @ApiOperation(value = "add answer to question",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> addAnswerToQuestion(@PathVariable int question_id, @RequestBody AnswerRequest answerRequest){
        AnswerResource answerResource = questionBusiness.addAnswerToQuestion(question_id, answerRequest);
        if (answerResource == null){
            return new ResponseEntity<>(answerResource, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(answerResource, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/admin/questions/{question_id}/answers")
    @ApiOperation(value="get All questions for a quiz",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<AnswerResource> getAllAnswersForQuestion(@PathVariable int question_id) {
        return questionBusiness.getAllAnswersForQuestion(question_id);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/admin/questions/{id}/answers")
    @ApiOperation(value="Update Answer List on Question",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public ResponseEntity<?> putAnswerList(@PathVariable int id, @RequestBody List<AnswerRequest> answerRequests){
        try{
            QuestionExtendedResource questionUpdated = questionBusiness.putAnswerList(id, answerRequests);
            return new ResponseEntity<>(questionUpdated, HttpStatus.OK);
//        }catch (ResourceNotFoundException e){
//            HashMap<String, String> response = new HashMap<>();
//            response.put("error", e.toString());
//            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }catch (Exception e){
            HashMap<String, String> response = new HashMap<>();
            response.put("error", e.toString());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


}
