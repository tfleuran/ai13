package com.ai13.groupe2.backend.domain.dao;


import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Data
@Entity
@Table(name = "question")
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "AggregateListQuestion", classes = {
                @ConstructorResult(targetClass = QuestionResource.class, columns = {
                        @ColumnResult(name = "id", type = Integer.class)
                        ,@ColumnResult(name = "label", type = String.class)
                        ,@ColumnResult(name = "position", type = Integer.class)
                        ,@ColumnResult(name = "status", type = Boolean.class)
                })
        }),
        @SqlResultSetMapping(name = "AggregateListIdQuestion", classes = {
                @ConstructorResult(targetClass = Integer.class, columns = {
                        @ColumnResult(name = "id", type = Integer.class)
                })
        })
})
public class Question {
    @Id
    @Column(name = "id", unique = true, nullable = false, insertable = false, updatable = false)
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;

    @Column (name = "label", nullable = false)
    private String label;

    @Column (name = "position")
    private int position;

    @Column (name = "status", nullable = false)
    private Boolean status;

    @ManyToOne
    @JoinColumn(name="id_quiz")
    private Quiz quiz;

    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY)
    private Collection<Answer> answers;

    public Question(){}

    public Question(String label, int position, Boolean status, Quiz quiz) throws Exception{
        if (label == null || quiz == null) throw new Exception("The label or quizz is needed");
        this.quiz = quiz;
        this.label = label;
        this.position = position;
        this.status = status == null? true : status;
    }

    public Question(QuestionRequest questionRequest){
        this.label = questionRequest.getLabel();
        this.position = questionRequest.getPosition();
        this.status = questionRequest.getStatus() == null? true : questionRequest.getStatus();
    }

    public boolean isActivable(){
        List<Answer> activeAndCorrectAnswers = answers.stream().filter(x -> x.getCorrect() && x.getStatus()).collect(Collectors.toList());
        return activeAndCorrectAnswers.size() == 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Collection<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
    }
}

