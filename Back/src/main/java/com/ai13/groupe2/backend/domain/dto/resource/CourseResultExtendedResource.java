package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Course;

import java.sql.Timestamp;
import java.time.Duration;
import java.util.List;

public class CourseResultExtendedResource {
    private int id;
    private int score;
    private Duration duration;
    private Timestamp creationDate;
    private List<CourseResultQuestionResource> results;

    public CourseResultExtendedResource() {
    }

//    public CourseResultResource(int id, int score, Duration duration, Timestamp creationDate) {
//        this.id = id;
//        this.score = score;
//        this.duration = duration;
//        this.creationDate = creationDate;
//    }

    public CourseResultExtendedResource(Course course, List<CourseResultQuestionResource> courseResultQuestionResource) {
        this.id = course.getId();
        this.score = course.getScore();
        this.duration = course.getDuration();
        this.creationDate = course.getCreationDate();
        this.results = courseResultQuestionResource;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getDuration() {
        return duration.toMillis();
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public List<CourseResultQuestionResource> getResults() {
        return results;
    }

    public void setResults(List<CourseResultQuestionResource> results) {
        this.results = results;
    }
}
