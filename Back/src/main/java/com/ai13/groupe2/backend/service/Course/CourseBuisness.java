package com.ai13.groupe2.backend.service.Course;

import com.ai13.groupe2.backend.domain.dto.request.CourseRequest;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResource;
import com.ai13.groupe2.backend.domain.dto.resource.CourseResultExtendedResource;

import java.util.List;
import java.util.Map;

public interface CourseBuisness {
    Map<String, String> addCourse(CourseRequest courseRequest, String username);

    List<CourseResource> getAllCourses(String username);

    CourseResultExtendedResource getCourseResultForUser(int id_course, String username) throws Exception;
}
