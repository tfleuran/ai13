package com.ai13.groupe2.backend.service.answer;

public enum AnswerSql {
    GET_ALL(
            "SELECT id, label, position, status " +
                    "FROM questions"
    ),
    GET_ALL_ACTIF(

            "SELECT id, label, position, status " +
                    "FROM questions" +
                    "WHERE statut = 1"
    );

    public String getQuery() {
        return query;
    }

    private String query;
    AnswerSql(String query){
        this.query = query;
    }
}
