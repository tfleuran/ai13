package com.ai13.groupe2.backend.service.subject;

import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.domain.dto.resource.SubjectResource;
import com.ai13.groupe2.backend.service.SharedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@SuppressWarnings("unchecked")
public class SubjectRepositoryImpl extends SharedRepository implements SubjectRepository {

    private SubjectJpaRepository subjectJpaRepository;

    @Autowired
    public SubjectRepositoryImpl(SubjectJpaRepository subjectJpaRepository) {
        this.subjectJpaRepository = subjectJpaRepository;
    }

    @Override
    public List<SubjectResource> getAllSubjects(){
        List<Subject> jpaResult = subjectJpaRepository.findAll();
        return jpaResult.stream().map(SubjectResource::new).collect(Collectors.toList());
    }

    @Override
    public Subject save(Subject subject) {
        return subjectJpaRepository.save(subject);
    }


}
