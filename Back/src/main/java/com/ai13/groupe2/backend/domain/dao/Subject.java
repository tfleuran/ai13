package com.ai13.groupe2.backend.domain.dao;

import com.ai13.groupe2.backend.domain.dto.resource.SubjectResource;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@Entity
@Table(name = "subject")
@SqlResultSetMapping(name = "AggregateListSubject", classes = {
        @ConstructorResult(targetClass = SubjectResource.class, columns = {
                @ColumnResult(name = "label", type = String.class)
        })
}) // Review: Might not be useful, as we don't need to filter attributes, but it can stay here for now
public class Subject implements BaseModel {

    @Id
    @Column(name = "label", unique = true, nullable = false)
    private String label;

    @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY)
    private Collection<Quiz> quizzes;

    public Subject(){}

    //TODO : Try @NotNull instead of Exception?
    public Subject(String label) throws Exception {
        if (label==null) throw new Exception("Missing mandatory fields to create subject");
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
