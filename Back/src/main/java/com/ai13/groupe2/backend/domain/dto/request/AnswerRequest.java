package com.ai13.groupe2.backend.domain.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Data;

//@NoArgsConstructor
@Data
//@Builder
@ApiModel(value="AnswerRequest", description="Answer Model DTO for Requests")
public class AnswerRequest {

    private int id;
    private String label;
    private int position;
    private Boolean status;
    private Boolean isCorrect;

    public AnswerRequest(){}

    public AnswerRequest(int id, String label, int position, Boolean status, Boolean isCorrect) throws Exception{
        this.id = id;
        if (label==null || status==null || isCorrect==null) throw new Exception("Missing mandatory fields to create user");
        this.label = label;
        this.position = position;
        this.status = status;
        this.isCorrect = isCorrect;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
