package com.ai13.groupe2.backend.service.answer;

import com.ai13.groupe2.backend.domain.dao.Answer;

import java.util.List;

public interface AnswerRepository {
    List<Answer> findAllAnswersByIdQuestion(int idQuestion);

    Answer save(Answer answer);

    int countByQuestion_IdAndIsCorrectAndStatus(int question, boolean isCorrect, boolean status);

}
