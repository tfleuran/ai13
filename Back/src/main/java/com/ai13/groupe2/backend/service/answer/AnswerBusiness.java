package com.ai13.groupe2.backend.service.answer;

import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;

import java.util.List;

public interface AnswerBusiness {
    List<AnswerResource> getAuuResponse();

    AnswerResource createAnswer(AnswerResource answerResource);
}
