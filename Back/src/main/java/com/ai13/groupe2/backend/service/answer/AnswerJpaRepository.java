package com.ai13.groupe2.backend.service.answer;

import com.ai13.groupe2.backend.domain.dao.Answer;
import com.ai13.groupe2.backend.domain.dao.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface AnswerJpaRepository extends JpaRepository<Answer, String> {
    Answer findById(int id);

    Collection<Answer> findAllByQuestion(Question q);

    int countByQuestion_IdAndIsCorrectAndStatus(int question, boolean isCorrect, boolean status);
}
