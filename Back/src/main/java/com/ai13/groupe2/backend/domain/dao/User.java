package com.ai13.groupe2.backend.domain.dao;

import com.ai13.groupe2.backend.domain.dto.request.UserUpdateRequest;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "users")
@SqlResultSetMapping(name = "AggregateList", classes = {
        @ConstructorResult(targetClass = UserResource.class, columns = {
                @ColumnResult(name = "email", type = String.class)
                ,@ColumnResult(name = "name", type = String.class)
                ,@ColumnResult(name = "company", type = String.class)
                ,@ColumnResult(name = "phone_number", type = String.class)
                ,@ColumnResult(name = "admin", type = Boolean.class)
                ,@ColumnResult(name = "status", type = Boolean.class)
                ,@ColumnResult(name = "creation_date", type = Timestamp.class)
        })
})
public class User implements BaseModel {

    @Id
    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="company")
    private String company;

    @Column(name="phone_number")
    private String phoneNumber;

    @Column(name="status", nullable = false)
    private boolean status; //TODO: faire un enum?

    @Column(name="admin", nullable = false)
    private boolean admin;

    @Column(name="creation_date", nullable = false)
    private Timestamp creationDate;

    public User(){}

    public User(String email, String password, String name, String company,
                String phoneNumber, boolean status, boolean admin) throws Exception {
        if (email==null || password==null || name==null) throw new Exception("Missing mandatory fields to create user");
        this.email = email;
        this.password = password;
        this.name = name;
        this.company = company;
        this.phoneNumber = phoneNumber;
        this.status = status;
        this.admin = admin;
        this.creationDate = Timestamp.valueOf(LocalDateTime.now());
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public void switchStatus(){
        this.status=!this.status;
    }

    public void userUpdate(String name, String company,
                           String phoneNumber) throws Exception{
        if(name==null) throw new Exception("Missing mandatory fields to update user");
        this.name = name;
        this.company = company;
        this.phoneNumber = phoneNumber;
    }

    public void userUpdate(UserUpdateRequest userUpdateRequest) throws Exception{
        if(userUpdateRequest.getName()==null) throw new Exception("Missing mandatory fields to update user");
        this.name = userUpdateRequest.getName();
        this.company = userUpdateRequest.getCompany();
        this.phoneNumber = userUpdateRequest.getPhoneNumber();
    }
}
