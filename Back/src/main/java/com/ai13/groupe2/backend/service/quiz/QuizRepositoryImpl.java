package com.ai13.groupe2.backend.service.quiz;

import com.ai13.groupe2.backend.domain.dao.Course;
import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.resource.*;
import com.ai13.groupe2.backend.service.Course.CourseJpaRepository;
import com.ai13.groupe2.backend.service.SharedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@SuppressWarnings("unchecked")
public class QuizRepositoryImpl extends SharedRepository implements QuizRepository {

    private static final String Quiz_LIST_RESULT_MAPPING = "AggregateListQuiz";
    private final QuizJpaRepository quizJpaRepository;
    private final CourseJpaRepository courseJpaRepository;

    @Autowired
    public QuizRepositoryImpl(QuizJpaRepository quizJpaRepository, CourseJpaRepository courseJpaRepository){
        this.quizJpaRepository = quizJpaRepository;
        this.courseJpaRepository = courseJpaRepository;
    }


    @Override
    public void addQuestionToQuiz(int idQuiz, QuestionRequest questionRequest) {

    }

    @Override
    public List<QuizResource> getAllQuizzes() {
//        Query query = entityManager.createNativeQuery(QuizSql.GET_ALL.getQuery(), Quiz_LIST_RESULT_MAPPING);
        return quizJpaRepository.findAll().stream().map(QuizResource::new).collect(Collectors.toList());
    }

    @Override
    public Quiz findById(int id) {
        return quizJpaRepository.findById(id);
    }

    @Override
    public void addQuiz(Quiz quiz) {
//        Subject subject = quiz.getSubject();
//        quiz.setSubject(entityManager.getReference(Subject.class, quiz.getSubject().getLabel()));
        quizJpaRepository.save(quiz);
    }

    @Override
    public Quiz getOne(int id) {
        return quizJpaRepository.getOne(id);
    }

    @Override
    public List<QuestionExtendedResource> getAllQuestionsForQuiz(int id) {
        Quiz quiz = quizJpaRepository.getOne(id);
        List<Question> questions = new ArrayList<>(quiz.getQuestions());
        List<QuestionExtendedResource> questionResources = questions.stream().map(QuestionExtendedResource::new).collect(Collectors.toList());
        return questionResources;
    }

    @Override
    public List<QuizResource> getAllActifQuizzes() {
        Query query = entityManager.createNativeQuery(QuizSql.GET_ALL_ACTIF.getQuery(), Quiz_LIST_RESULT_MAPPING);
        return query.getResultList();
    }

    @Override
    public QuizExtendedResource getQuizWithActiveQuestions(int id){
        Quiz quiz = quizJpaRepository.getOne(id);
        QuizExtendedResource quizResource = new QuizExtendedResource(quiz);
        quizResource.setQuestions(quizResource.getQuestions().stream().filter(QuestionExtendedResource::getStatus).collect(Collectors.toList()));
        for (QuestionExtendedResource question : quizResource.getQuestions()) {
            question.setAnswers(question.getAnswers().stream().filter(AnswerResource::getStatus).collect(Collectors.toList()));
        }
        return quizResource;
    }

    @Override
    public List<QuizResource> getAvailableQuizzes(String user){
        List<Course> passedCourses = courseJpaRepository.findAllByUserEmail(user);
        List<QuizResource> availableQuizzes = new ArrayList<>();
        Query query = entityManager.createNativeQuery(QuizSql.GET_ALL_ACTIF.getQuery(), Quiz_LIST_RESULT_MAPPING);
        List<QuizResource> allQuizzes = query.getResultList();
        for(QuizResource quiz : allQuizzes) {
            boolean passed = false;
            for (Course course : passedCourses) {
                if(quiz.getId() == course.getQuiz().getId())
                    passed = true;
            }
            if(!passed)
                availableQuizzes.add(quiz);
        }
        return availableQuizzes;
    }
}
