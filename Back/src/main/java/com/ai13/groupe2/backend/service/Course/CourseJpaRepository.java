package com.ai13.groupe2.backend.service.Course;

import com.ai13.groupe2.backend.domain.dao.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseJpaRepository extends JpaRepository<Course, Integer> {
    List<Course> findAllByUserEmail(String user);
    Course findOneById(int id);
}
