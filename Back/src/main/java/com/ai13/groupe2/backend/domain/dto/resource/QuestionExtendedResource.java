package com.ai13.groupe2.backend.domain.dto.resource;

import com.ai13.groupe2.backend.domain.dao.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class QuestionExtendedResource {
    private int id;
    private String label;
    private int position;
    private boolean status;
//    private int id_quiz;
    private List<AnswerResource> answers;
    private boolean activable;

//    public QuestionExtendedResource(Integer id, String label, Integer position, Boolean status) {
//        this.id = id;
//        this.label = label;
//        this.position = position;
//        this.status = status;
//    }

    public QuestionExtendedResource(Question question){
        this.id = question.getId();
        this.label = question.getLabel();
        this.position = question.getPosition();
        this.status = question.getStatus();
        this.answers = question.getAnswers().stream().map(AnswerResource::new).collect(Collectors.toList());
        this.activable = question.isActivable();
    }
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<AnswerResource> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerResource> answers) {
        this.answers = answers;
    }

    public boolean isActivable() {
        return activable;
    }

    public void setActivable(boolean activable) {
        this.activable = activable;
    }
}
