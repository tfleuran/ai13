package com.ai13.groupe2.backend.service.quiz;


import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizResource;

import java.util.List;

public interface QuizRepository {
    Quiz findById(int id);

    List<QuizResource> getAllQuizzes();

    void addQuestionToQuiz(int idQuiz, QuestionRequest questionRequest);

    void addQuiz(Quiz Quiz);

    Quiz getOne(int id);

    List<QuestionExtendedResource> getAllQuestionsForQuiz(int id);

    List<QuizResource> getAllActifQuizzes();

    QuizExtendedResource getQuizWithActiveQuestions(int id);

    List<QuizResource> getAvailableQuizzes(String user);
}
