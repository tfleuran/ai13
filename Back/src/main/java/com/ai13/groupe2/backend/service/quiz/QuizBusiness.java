package com.ai13.groupe2.backend.service.quiz;

import com.ai13.groupe2.backend.domain.dto.request.QuestionRequest;
import com.ai13.groupe2.backend.domain.dto.request.QuizRequest;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuizResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

public interface QuizBusiness {
    List<QuizResource> getAllQuizzes();

    QuizResource patchStatus(int id) throws ResourceNotFoundException, Exception;

    QuestionResource addQuestionToQuiz(int id, QuestionRequest questionRequest);

    Map<String, String> addQuiz(QuizRequest formDTO);

    List<QuestionExtendedResource> getAllQuestionsForQuiz(int id);

    List<QuizResource> getAllActifQuizzes();

    QuizExtendedResource getQuizWithActiveQuestions(int id);

    List<QuizResource> getAvailableQuizzes(String user);
}
