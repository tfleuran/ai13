package com.ai13.groupe2.backend.service.question;

import com.ai13.groupe2.backend.domain.dao.Answer;
import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dto.request.AnswerRequest;
import com.ai13.groupe2.backend.domain.dto.resource.AnswerResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionExtendedResource;
import com.ai13.groupe2.backend.domain.dto.resource.QuestionResource;

import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import com.ai13.groupe2.backend.service.answer.AnswerRepository;
import com.google.common.collect.Iterables;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuestionBusinessImpl implements QuestionBusiness {

    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    public QuestionBusinessImpl(QuestionRepository questionRepository, AnswerRepository answerRepository) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    @Override
    public List<QuestionResource> getAllQuestions() {
        return questionRepository.getAllQuestions();
    }

    @Override
    public List<QuestionResource> getActiveQuestions() {
        return questionRepository.getActiveQuestions();
    }

    @Override
    public QuestionResource putLabel(int id, String newLabel) throws ResourceNotFoundException {
        Question questionToUpdate = questionRepository.getQuestionById(id);
        if (questionToUpdate == null)
            throw new ResourceNotFoundException("Question not found");
        else {
            questionToUpdate.setLabel(newLabel);
            return new QuestionResource(questionToUpdate);
        }
    }

    @Override
    public QuestionResource patchStatus(int id) throws ResourceNotFoundException, Exception {
        Question questionToUpdate = questionRepository.getQuestionById(id);
        if (questionToUpdate == null)
            throw new ResourceNotFoundException("Question not found");
        else {
            questionToUpdate.setStatus(!questionToUpdate.getStatus());
            return new QuestionResource(questionToUpdate);
        }
    }


    @Override
    public AnswerResource addAnswerToQuestion(int id, AnswerRequest answerRequest) {
        Answer answerToCreate = new Answer(answerRequest);
        if (answerRequest.getCorrect()) {
            List<Answer> answers = answerRepository.findAllAnswersByIdQuestion(id); //TODO: Change to List<Answer>, as it is a working object
            for (Answer answer : answers) {
                if (answer.getCorrect()) {
                    return null;
                }
            }
        }
        Question question = questionRepository.getQuestionById(id);
        answerToCreate.setQuestion(question);

        Answer answerSaved = answerRepository.save(answerToCreate);
        return new AnswerResource(answerSaved);

    }

    @Override
    public List<AnswerResource> getAllAnswersForQuestion(int question_id) {
        return questionRepository.getAllAnswersForQuestion(question_id);
    }

    @Override
    public QuestionExtendedResource putAnswerList(int id, List<AnswerRequest> answerRequests) {
        Question question = questionRepository.getQuestionById(id);
        // TODO : Throw Exception if Null
        // TODO : Throw exception if More than One Active Answer with correct = true
        // TODO: ResourceNotFoundException

        //Compare which Answers are present or absent from Request
        List<Integer> answersIdAlreadyOnQuestion = question.getAnswers().stream().map(Answer::getId).collect(Collectors.toList());
        List<Integer> answersIdInAnswerRequests = answerRequests.stream().filter(x -> x.getId() != 0).map(AnswerRequest::getId).collect(Collectors.toList());

        //Disable absent Answers
        List<Integer> answersIdToDisable = answersIdAlreadyOnQuestion.stream().filter(x -> !answersIdInAnswerRequests.contains(x)).collect(Collectors.toList());
        List<Answer> answersToDisable = question.getAnswers().stream().filter(answer -> answersIdToDisable.contains(answer.getId())).collect(Collectors.toList());

        answersToDisable = answersToDisable.stream().peek((Answer answer) -> answer.setStatus(false)).collect(Collectors.toList());

        //Create new Answers :
        List<Answer> answersNew = answerRequests.stream().filter(x -> x.getId() == 0).map(x -> {
            Answer answer = new Answer(x);
            answer.setQuestion(question);
            return answerRepository.save(answer);
        }).collect(Collectors.toList());

        //Update Answers from answerRequests which already existed
        List<AnswerRequest> answerRequestForUpdate = answerRequests.stream().filter(x -> x.getId() != 0).collect(Collectors.toList());
        List<Answer> answerToUpdateFromAnswerRequest = question.getAnswers()
                .stream()
                .filter(x -> answerRequestForUpdate.stream().map(AnswerRequest::getId).collect(Collectors.toList()).contains(x.getId()))
                .peek((Answer answer) -> {
                    AnswerRequest answerRequest = answerRequestForUpdate.stream().filter(x -> x.getId() == answer.getId()).reduce((a, b) -> {
                        throw new IllegalStateException("Multiple elements: " + a + ", " + b);
                    }).get();

                    answer.updateFromAnswerRequest(answerRequest);
                })
                .collect(Collectors.toList());

        // Make a single list of question
        List<Answer> finalAnswerList = new ArrayList<>();
        finalAnswerList.addAll(answersToDisable);
        finalAnswerList.addAll(answersNew);
        finalAnswerList.addAll(answerToUpdateFromAnswerRequest);

        // Update Question
        question.setAnswers(finalAnswerList);

        return new QuestionExtendedResource(question);
    }

}
