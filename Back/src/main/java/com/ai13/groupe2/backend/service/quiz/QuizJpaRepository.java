package com.ai13.groupe2.backend.service.quiz;

import com.ai13.groupe2.backend.domain.dao.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizJpaRepository extends JpaRepository<Quiz, Integer> {
    Quiz findById(int id);
}
