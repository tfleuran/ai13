package com.ai13.groupe2.backend.controller;


import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.domain.dto.request.SubjectRequest;
import com.ai13.groupe2.backend.domain.dto.resource.SubjectResource;
import com.ai13.groupe2.backend.domain.dto.resource.UserResource;
import com.ai13.groupe2.backend.exception.ResourceNotFoundException;
import com.ai13.groupe2.backend.service.subject.SubjectBusiness;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Api(tags = "Subjects"
)
public class SubjectController {

    private final SubjectBusiness subjectBusiness;

    @Autowired
    public SubjectController(SubjectBusiness subjectBusiness) {
        this.subjectBusiness = subjectBusiness;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/subjects")
    @ApiOperation(value="Get All Subjects",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<SubjectResource> getAllSubjects() {
        return subjectBusiness.getAllSubjects();
    }

    // useless for now, but we may use this in the future
    /*@PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/subjects/search")
    @ApiOperation(value="Get All Subjects corresponding exactly to provided criterias",
            authorizations = {
                    @Authorization(value = "Bearer")
            }
    )
    public List<SubjectResource> searchSubjectsByTerms(@RequestHeader String authorization, @ApiParam("Subject") @RequestBody SubjectRequest subject) {
        return subjectBusiness.searchSubjectsByTerms(subject);
    }*/

}
