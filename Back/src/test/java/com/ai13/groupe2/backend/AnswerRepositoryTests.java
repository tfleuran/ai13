package com.ai13.groupe2.backend;

import com.ai13.groupe2.backend.domain.dao.Answer;
import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.service.answer.AnswerJpaRepository;
import com.ai13.groupe2.backend.service.question.QuestionJpaRepository;
import com.ai13.groupe2.backend.service.quiz.QuizJpaRepository;
import com.ai13.groupe2.backend.service.subject.SubjectJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations= "classpath:test.properties")
public class AnswerRepositoryTests {

    @Autowired
    private SubjectJpaRepository subjectJpaRepository;

    @Autowired
    private QuizJpaRepository quizJpaRepository;

    @Autowired
    private QuestionJpaRepository questionJpaRepository;

    @Autowired
    private AnswerJpaRepository answerJpaRepository;

    @Test
    public void whenFindById_thenReturnAnswer() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);
        Quiz test_quiz = new Quiz("Test_quiz_question_rep_1", "desc", true, test_subject);
        quizJpaRepository.save(test_quiz);
        Question test_question = new Question("Test_Question", 1, true, test_quiz);
        questionJpaRepository.save(test_question);
        Answer test_answer = new Answer("Test_Answer_1", 1, true, false, test_question);
        answerJpaRepository.save(test_answer);

        // when
        Answer found = answerJpaRepository.findById(test_answer.getId());

        // then
        assertThat(found.getId())
                .isEqualTo(test_answer.getId());
    }

    @Test
    public void givenOneQuizSeveralQuestion_whenGetQuestionQuiz_thenReturnQuestions() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);
        Quiz test_quiz = new Quiz("Test_quiz_question_rep_1", "desc", true, test_subject);
        quizJpaRepository.save(test_quiz);
        Question test_question = new Question("Test_Question_X", 1, true, test_quiz);
        questionJpaRepository.save(test_question);
        Answer test_answer_A = new Answer("Test_Answer_A", 1, true, false, test_question);
        answerJpaRepository.save(test_answer_A);
        Answer test_answer_B = new Answer("Test_Answer_B", 1, true, false, test_question);
        answerJpaRepository.save(test_answer_B);

        // when
        Collection<Answer> found = answerJpaRepository.findAllByQuestion(test_question);
        Question question_comp = questionJpaRepository.findById(test_question.getId());

        // then
        Collection<Integer> id_found = found.stream().map(Answer::getId).collect(Collectors.toList());
        Collection<Integer> id_quiz_questions = question_comp.getAnswers().stream().map(Answer::getId).collect(Collectors.toList());

        assert id_found.containsAll(id_quiz_questions) && id_quiz_questions.containsAll(id_found);
    }
}
