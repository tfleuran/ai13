package com.ai13.groupe2.backend;

import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.service.quiz.QuizJpaRepository;
import com.ai13.groupe2.backend.service.subject.SubjectJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations= "classpath:test.properties")
public class QuizRepositoryTests {

    @Autowired
    private SubjectJpaRepository subjectJpaRepository;

    @Autowired
    private QuizJpaRepository quizJpaRepository;


    @Test
    public void whenFindById_thenReturnQuiz() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);
        Quiz test_quiz = new Quiz("Test_quiz", "desc", true, test_subject);
        quizJpaRepository.save(test_quiz);

        // when
        Quiz found = quizJpaRepository.findById(test_quiz.getId());

        // then
        assertThat(found.getId())
                .isEqualTo(test_quiz.getId());
    }

    @Test
    public void givenOneSubject_whenInsertSeveralQuiz_thenItShouldInsert() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);
        Quiz test_quiz_1 = new Quiz("Test_quiz_1", "desc", true, test_subject);

        Quiz test_quiz_2 = new Quiz("Test_quiz_2", "desc", true, test_subject);


        // when
        quizJpaRepository.save(test_quiz_1);
        quizJpaRepository.save(test_quiz_2);

        // then
        assert true;
    }
}
