package com.ai13.groupe2.backend;

import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.service.subject.SubjectJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
//import org.assertj.core.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations= "classpath:test.properties")
public class SubjectRepositoryTests {

    @Autowired
    private SubjectJpaRepository subjectJpaRepository;


    @Test
    public void whenFindByLabel_thenReturnSubject() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);

        // when
        Subject found = subjectJpaRepository.findByLabel(test_subject.getLabel());

        // then
        assertThat(found.getLabel())
                .isEqualTo(test_subject.getLabel());
    }
}
