package com.ai13.groupe2.backend;

import com.ai13.groupe2.backend.domain.dao.Question;
import com.ai13.groupe2.backend.domain.dao.Quiz;
import com.ai13.groupe2.backend.domain.dao.Subject;
import com.ai13.groupe2.backend.service.question.QuestionJpaRepository;
import com.ai13.groupe2.backend.service.quiz.QuizJpaRepository;
import com.ai13.groupe2.backend.service.subject.SubjectJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations= "classpath:test.properties")
public class QuestionRepositoryTests {

    @Autowired
    private SubjectJpaRepository subjectJpaRepository;

    @Autowired
    private QuizJpaRepository quizJpaRepository;

    @Autowired
    private QuestionJpaRepository questionJpaRepository;


    @Test
    public void whenFindById_thenReturnQuestion() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);
        Quiz test_quiz = new Quiz("Test_quiz_question_rep_1", "desc", true, test_subject);
        quizJpaRepository.save(test_quiz);
        Question test_question = new Question("Test_Question", 1, true, test_quiz);
        questionJpaRepository.save(test_question);

        // when
        Question found = questionJpaRepository.findById(test_question.getId());

        // then
        assertThat(found.getId())
                .isEqualTo(test_question.getId());
    }

    @Test
    public void givenOneQuizSeveralQuestion_whenGetQuestionQuiz_thenReturnQuestions() throws Exception {
        // given
        Subject test_subject = new Subject("Test Subject");
        subjectJpaRepository.save(test_subject);
        Quiz test_quiz = new Quiz("Test_quiz_question_rep_1", "desc", true, test_subject);
        quizJpaRepository.save(test_quiz);
        Question test_question_A = new Question("Test_Question_A", 1, true, test_quiz);
        questionJpaRepository.save(test_question_A);
        Question test_question_B = new Question("Test_Question_B", 1, true, test_quiz);
        questionJpaRepository.save(test_question_B);

        // when
        Collection<Question> found = questionJpaRepository.findAllByQuiz(test_quiz);
        Quiz quiz_comp = quizJpaRepository.findById(test_quiz.getId());

        // then
        Collection<Integer> id_found = found.stream().map(Question::getId).collect(Collectors.toList());
        Collection<Integer> id_quiz_questions = quiz_comp.getQuestions().stream().map(Question::getId).collect(Collectors.toList());

        assert id_found.containsAll(id_quiz_questions) && id_quiz_questions.containsAll(id_found);
    }


}
